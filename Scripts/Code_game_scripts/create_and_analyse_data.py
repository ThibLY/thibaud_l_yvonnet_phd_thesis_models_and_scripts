#!/usr/bin/env python

"""
Coordinate the creation of readable result from Prism.

This program aims first to compile all results of
Run_experiment into one file easily handable. it is to
called with 3 environments variables in the following order:
folder name, file generic name and number of
experimentations. Then, it plots the frequency and tries to
match it with a gaussian.
"""
import sys

from python_modules.file_and_csv_manipulator import\
                            CsvManipulator,\
                            DataFrameManipulator,\
                            TxtManipulator,\
                            PlotManipulator
from python_modules.analyse_statistics import\
                            DataFrameAnalyser


sys.path.insert(
    0,
    '/user/tlyvonne/home/Desktop/scripts/python_modules')
# This line must be changed accordingly to the directory
# in which you plce these scripts


def main():
    """Execute all task to beperformed for readable data."""
    print "Program Analyse_Prism_experiment.py launched"
    parameters = {}
    index = 0
    for arg in sys.argv:
        if index == 1:
            parameters["folder"] = arg
        elif index == 2:
            parameters["file"] = arg
        elif index == 3:
            parameters["quantity"] = int(arg)
        index += 1
    csv_manip = CsvManipulator(
        parameters["folder"],
        parameters["file"],
        parameters["quantity"])
    csv_all = csv_manip.opener()
    data_manip = DataFrameManipulator()
    for csv in csv_all:
        data_manip.add_data_to_my_frame(csv_all[csv])
    data_frame_result = data_manip.return_data_frame()
    data_analysis = DataFrameAnalyser(data_frame_result)
    csv_manip.add_data_frame_and_detail(
        [data_frame_result,
         data_analysis.return_data_frame()],
        ["Raw_results", "Frequency_results"])
    csv_manip.saver()
    print "Concatenation and frequency saved."
    stat_to_write = data_analysis.return_stats()
    text_to_save = "This experiment of "\
        + str(parameters["quantity"]) + " modeled patients"\
        + " led to the obtention of the following results"\
        + " : \n\nmu = " + str(stat_to_write[0])\
        + "\nsigma = " + str(stat_to_write[2])\
        + "\nvariance = " + str(stat_to_write[1])
    TxtManipulator(
        parameters["folder"],
        "Statistics",
        text_to_save)
    plot_to_save = data_analysis.return_plot()
    plot_manip = PlotManipulator(
        parameters["folder"],
        "Freq_and_Gauss",
        plot_to_save)
    plot_manip.saver()
    print "Freq_and_Gauss figure saved."


if __name__ == "__main__":
    main()
