#!/bin/bash

#===========================================================
# HEADER
#===========================================================
#% SYNOPSIS
#+    $./runPrismExperiment.sh modelFileName
#+        propertyFileName idProperty quantityOfExperiments
#%
#% DESCRIPTION
#%    This script launch some Prism experiments with a
#%        single simulation to retrieve all result.
#%        Afterward it calls a python script to make it
#%        readable and extract a histogram. Works only with
#%        file of extension .pm for the model and .pctl
#%        for the properties.
#%
#% parameters
#%    modelFileName         Path of the model's file without
#%                            extension.
#%    propertyFileName      Path of the properties file
#%                            without extension.
#%                            If the path is the same as the
#%                            model, use keyword "idem".
#%    idProperty            Id number of the wanted property
#%                            (position in the list of
#%                            properties)
#%    quantityOfExperiments Size of the wanted sample.
#%
#% EXAMPLES
#%    $./runPrismExperiment.sh My_Models/Serious_game
#%        idem 24 300
#%
#================================================================
#- IMPLEMENTATION
#-    version         $runPrismExperiment.sh 0.0.1
#-    author          Thibaud L'YVONNET
#-
#================================================================
#  HISTORY
#     21/03/2019 : Script creation
#================================================================
# END_OF_HEADER
#================================================================
Path1=$1
Path2=$2
if [ $Path2 == "idem" ]
then
  Path2=$Path1
fi

NumProp=$3
NumbExp=$4

echo "Program $0 launched."

while [ -z "$ResultDirectory" ] || [[ "Yy" == *"$Answer"* ]];
do
  Answer="q"
  read -p 'In which directory do you want to place your result? (Default will be Results)' ResultDirectory
  if [ "$ResultDirectory" = "" ];
  then
    ResultDirectory="Results"
  fi

  if [ -d "$ResultDirectory" ];
  then

    read -p '$ResultDirectory already exists, do you want to rename it? (If no, a number is added behind the name) Y/N ' Answer

    while [[ "YyNn" != *"$Answer"* ]];
    do
      read -p 'You should answer using Y or y for yes or N or n for no : ' Answer
    done

    if [[ "Nn" == *"$Answer"* ]];
    then
      Counter=1
      while [ -d "$ResultDirectory$Counter" ]; do
        Counter=$(( $Counter + 1 ))
      done
      ResultDirectory=$ResultDirectory$Counter
    fi
  fi
done

mkdir $ResultDirectory

read -p 'What should be the shared name for the results? (Default will be Result)' Resultfile
if [ "$Resultfile" = "" ];
then
  Resultfile="Result"
fi

Resultfile=$Resultfile"_"

progressionBar=______________________________ #length 30
if [ $NumbExp -ge 30 ]; then
  progressCounter=0
  for identifier in `seq 1 $NumbExp`;
  do
    $HOME/Software/prism-4.6-linux64/bin/prism $HOME/Desktop/Prism/$Path1.pm $HOME/Desktop/Prism/$Path2.pctl -prop $NumProp -const i=0:1:105 -sim -simmethod ci -simsamples 1 -exportresults $ResultDirectory/$Resultfile$identifier.txt:csv &>/dev/null
    #The line above is using the Prism command to run experiments.
    #I decided to store every prism model under a directory called Prism in Desktop.
    #In this line, you can modify the number of step performed by the simulation i=0:1:105
    progressCounter=`expr "$progressCounter" + 1`
    if [ $progressCounter -ge `expr "$NumbExp" / 30` ]; then
      progressCounter=0
      progressionBar=${progressionBar::-1}
      progressionBar="${progressionBar:0:0}=${progressionBar:0}"
    fi
    echo -ne "$progressionBar    $identifier/$NumbExp file(s) created \r"
  done
else
  progressFactor=`expr 30 / "$NumbExp"`
  for identifier in `seq 1 $NumbExp`;
  do
    $HOME/Software/prism-4.6-linux64/bin/prism $HOME/Desktop/Prism/$Path1.pm $HOME/Desktop/Prism/$Path2.pctl -prop $NumProp -const i=0:1:105 -sim -simmethod ci -simsamples 1 -exportresults $ResultDirectory/$Resultfile$identifier.txt:csv &>/dev/null
    #The line above is using the Prism command to run experiments.
    #I decided to store every prism model under a directory called Prism in Desktop.
    #percent=$(awk "BEGIN { pc=100*${identifier}/${NumbExp}; i=int(pc); print (pc-i<0.5)?i:i+1 }")
    progressionBar=${progressionBar::-$progressFactor}
    for progress in `seq 1 $progressFactor`;
    do
      progressionBar="${progressionBar:0:0}=${progressionBar:0}"
    done
    echo -ne "$progressionBar    $identifier/$NumbExp file(s) created \r"
  done
fi
echo "$progressionBar    $identifier/$NumbExp file(s) created"

mkdir $ResultDirectory/Figures

echo 'Result files created, concatenation triggered.'

./create_and_analyse_data.py $ResultDirectory $Resultfile $NumbExp

echo 'Operation done, program terminated'
