#!/usr/bin/env python

"""
In this module, You can find the Class DataFrameAnalyser.

The aim of this class is to create a histogram and the
matching gaussian curve.
"""

import pandas as pd
import numpy as np
import scipy.stats as stats
import matplotlib.mlab as mlab
from matplotlib import pyplot as plt

import python_modules.basic_operation as bop


class DataFrameAnalyser(object):
    """docstring for ."""

    def __init__(self, raw_frame):
        """
        Initialise Class DataFrameAnalyser.

        Creates diagrames and statistics.
        """
        self.frequency_frame = None
        self.create_frequency_frame(raw_frame)
        self.frequency_frame.reset_index(
            level=0, inplace=True)
        self.my_list = self.create_list()
        (self.mean_my_list,
         self.sigma_my_list) = stats.norm.fit(
             self.my_list)
        self.variance_my_list = self.calculate_variance()
        self.subplot_freq = None
        self.subplot_gauss = None
        self.plot_gaussian_and_frequency()

    def create_frequency_frame(self, raw_frame):
        """
        Create a frequency data frame.

        This methode creates a data frame that specifies the
        number of iteration amongs the whole set of modeled
        patients described in raw_frame.

        Args:
            raw_frame is the data frame created out of Prism
            result.
        """
        serie = raw_frame.iloc[-1]
        serie = serie.iloc[1:]
        new_dict = serie.value_counts().to_dict()
        self.frequency_frame = pd.DataFrame.from_dict(
            new_dict, orient='index')

    def create_list(self):
        """
        Create a list of resultsself.

        This list is created according to the frequency
        frame previously created.
        """
        column_header = []
        for elem in self.frequency_frame:
            column_header += [elem]
        data_list = []
        for i in range(0, len(self.frequency_frame)):
            for _ in range(
                        0, int(
                            self.frequency_frame.iloc[i][
                                column_header[1]])):
                data_list += [self.frequency_frame.iloc[i][
                    column_header[0]]]
        return data_list

    def calculate_variance(self):
        """Calculate the variance of self.my_listself."""
        return np.var(self.my_list)

    def plot_gaussian_and_frequency(self):
        """
        Create a figure with 2 plots : Frequence & Gaussian.

        The frequence is created with self.my_list while the
        gaussian is created withe the mean and the square
        root of the variance of this list.
        """
        self.subplot_freq = plt.subplot()
        # preparing the first plot
        hist_returns = self.subplot_freq.hist(
            self.my_list, np.arange(
                min(self.my_list),
                max(self.my_list))-0.5,
            facecolor='dodgerblue',
            edgecolor='black',
            alpha=0.75)
        # np.arange()-0.5 allows us to center bar of hist on
        # each value of x axis
        gauss_func = mlab.normpdf(hist_returns[1],
                                  self.mean_my_list,
                                  self.sigma_my_list)
        # equation for the Gaussian
        self.subplot_gauss = self.subplot_freq.twinx()
        # we create a twinx() to hav both plots on same plan
        self.subplot_gauss.plot(
            hist_returns[1], gauss_func, 'red', linewidth=3)
        # plot gaussian
        top_tick = round(self.subplot_freq.get_ybound()[1])
        top_tick, tick_granul = bop.tick_granularity(
            top_tick)
        # to create the right number of tick on the y axis,
        # we go through tick_granularity()
        self.subplot_freq.set_yticks(
            np.linspace(0, top_tick, tick_granul))
        self.subplot_freq.set_ylim(
            0, top_tick)
        self.subplot_freq.tick_params(labelsize=12)
        self.subplot_gauss.set_yticks(
            np.linspace(0,
                        top_tick/len(self.my_list),
                        tick_granul))
        self.subplot_gauss.set_ylim(
            0, self.subplot_gauss.get_ylim()[1])
        self.subplot_gauss.tick_params(labelsize=12)
        # set the ticks on y axis
        self.subplot_freq.yaxis.grid(True, which='both')
        self.subplot_freq.set_xlabel(
            "Final Number of good answers", fontsize=16)
        self.subplot_freq.set_ylabel(
            "Number of modeled patients", fontsize=16)
        self.subplot_gauss.set_ylabel(
            "Proportion of patients", fontsize=16)
        # put label and grill
        plt.xticks(
            range(
                int(min(hist_returns[1])),
                int(max(hist_returns[1]))+1))
        # set the ticks on x axis

    def return_data_frame(self):
        """Return frequency_frame."""
        return self.frequency_frame

    def return_stats(self):
        """Return the mean, variance and sigma."""
        return (self.mean_my_list,
                self.variance_my_list,
                self.sigma_my_list)

    def return_plot(self):
        """Return the plots."""
        return self.subplot_freq
