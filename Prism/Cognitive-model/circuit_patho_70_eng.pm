dtmc

module Time
time:[0..10];
[to] time<10 -> 1:(time'=time+1);
[to] time=10 -> 1:(time'=1);
endmodule

module entry
//Input spike that follow a Poissonian law with 7 as parameter
//Prism does not support more than 15 digit after the dot for this command
t1:[0..3000];
[to] true -> 0.000072991952613:(t1'=1) + 0.003437086558390:(t1'=2) + 0.021604031452484:(t1'=3) + 0.059540362609726:(t1'=4) + 0.104444862957054:(t1'=5) + 0.137676978041126:(t1'=6) + 0.149002779674338:(t1'=7) + 0.139586531950597:(t1'=8) + 0.117116124452909:(t1'=9) + 0.26751825035076304:(t1'=10);
endmodule

const int wsnpcstr=30;
const int tau=80;
const double r=0.5;
const int wcxstr=80;
const int wstrgpe=40;
const int wgpestr=80;
const int wgpegpe=20;
const int wstngpe=100;
const int wgpestn=40;
const int wstrsnr=40;
const int wgpesnr=20;
const int wstnDelay=80;
const int wDelaysnr=100;
const int wsnrth=80;
const int wcxth=80;
const int wstop=80;

//Global membrane potential formula for STr
formula STr_healthy=floor((-wsnpcstr*t1)+(wcxstr*t1)+(-wgpestr*n_GPe)+(r*potential_STr*(1-(floor(potential_STr/tau)/10))));
formula STr_patho=floor((wcxstr*t1)+(-wgpestr*n_GPe)+(r*potential_STr*(1-(floor(potential_STr/tau)/10))));

//Global membrane potential formula for GPe
formula GPe=floor((-wstrgpe*n_STr)+(wstngpe*n_STN)+(wgpegpe*n_GPe)+(r*potential_GPe*(1-(floor(potential_GPe/tau)/10))));

//Global membrane potential formula for STN
formula STN=floor((-wgpestn*n_GPe)+(r*potential_STN*(1-(floor(potential_STN/tau)/10))));

//Global membrane potential formula for SNr
formula SNr=floor(-((wstrsnr*n_STr)+(wgpesnr*n_GPe))+(wDelaysnr*n_Delay)+(r*potential_SNr*(1-(floor(potential_SNr/tau)/10))));

//Global membrane potential formula for Delay
formula Delay=floor(wstnDelay*n_STN);

//Global membrane potential formula for Th
formula Th=floor((-wsnrth*n_SNr)+(wcxth*t1)+(r*potential_Th*(1-(floor(potential_Th/tau)/10))));

module STr
potential_STr:[0..800];
n_STr:[0..10];
[to] n_STr>=0 -> 0.3:(n_STr'=STr_healthy<0?0:STr_healthy>800?10:floor(STr_healthy/tau)) & (potential_STr'=STr_healthy<0?0:STr_healthy>800?800:STr_healthy) + 0.7:(n_STr'=STr_patho<0?0:STr_patho>800?10:floor(STr_patho/tau)) & (potential_STr'=STr_patho<0?0:STr_patho>800?800:STr_patho);
endmodule

module GPe
potential_GPe:[0..800];
n_GPe:[0..10];
[to] n_GPe>=0 -> 1:(n_GPe'=GPe<0?0:GPe>800?10:floor(GPe/tau)) & (potential_GPe'=GPe<0?0:GPe>800?800:GPe);
endmodule

module STN
potential_STN:[0..800];
n_STN:[0..10];
[to] time !=9 & n_STN>=0 -> 1:(n_STN'=STN<0?0:STN>800?10:floor(STN/tau)) & (potential_STN'=STN<0?0:STN>800?800:STN);
//Here is the update that represent the stop signal input
[to] time=9 -> 1:(potential_STN'=800) & (n_STN'=10);
//
endmodule

module SNr
potential_SNr:[0..800];
n_SNr:[0..10];
[to] n_SNr>=0 -> 1:(n_SNr'=SNr<0?0:SNr>800?10:floor(SNr/tau)) & (potential_SNr'=SNr<0?0:SNr>800?800:SNr);
endmodule

module Delay
potential_Delay:[0..800];
n_Delay:[0..10];
[to] n_Delay>=0 -> 1:(n_Delay'=Delay<0?0:Delay>800?10:floor(Delay/tau)) & (potential_Delay'=Delay<0?0:Delay>800?800:Delay);
endmodule

module Th
potential_Th:[0..800];
n_Th:[0..10];
[to] n_Th>=0 -> 1:(n_Th'=Th<0?0:Th>800?10:floor(Th/tau)) & (potential_Th'=Th<0?0:Th>800?800:Th);
endmodule

rewards "Inhibited"
	n_Th < 4 & time > 0 :1;
endrewards
