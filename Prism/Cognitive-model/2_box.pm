dtmc

module Entry
//Input spike that follow a Poissonian law with 7 as parameter
//Prism does not support more than 15 digit after the dot for this command
t1:[0..10];
[to] true -> 0.000072991952613:(t1'=1) + 0.003437086558390:(t1'=2)
+ 0.021604031452484:(t1'=3) + 0.059540362609726:(t1'=4) + 0.104444862957054:(t1'=5)
+ 0.137676978041126:(t1'=6) + 0.149002779674338:(t1'=7) + 0.139586531950597:(t1'=8)
+ 0.117116124452909:(t1'=9) + 0.26751825035076304:(t1'=10);
endmodule

//tau => Spike activition threshold
const tau=80;
//r => Leak factor
const double r=0.5;
//Input weight of each box
const entry=80;
const b2b1=-50;
const b1b2=90;

//Global membrane potential formula for B1
formula b1=floor((entry*t1)+(b2b1*nb2)+(r*pb1*(1-(floor(pb1/tau)/10))));

//Global membrane potential formula for B2
formula b2=floor((b1b2*nb1)+(r*pb2*(1-(floor(pb2/tau)/10))));

module B1
pb1:[0..800];//Global membrane potential of B1 at step t, maximum value is 800
nb1:[0..10]; //Total number of output spikes of B1 at step t, maximum value is 10
//If pb1 goes under 0, both nb1 and pb1 are resetted to 0
//Or if pb1>tau*10, nb1 is set to 10 and pb1 to 800
//Thus, the Prism set of guard/update is the following:
[to] true-> 1:(nb1'=b1<0?0:b1>800?10:floor(b1/tau)) & (pb1'=b1<0?0:b1>800?800:b1);
endmodule

module B2
pb2:[0..800];//Global membrane potential of B2 at step t, maximum value is 800
nb2:[0..10]; //Total number of output spikes of B2 at step t, maximum value is 10
//If pb2 goes under 0, both nb2 and pb2 are resetted to 0
//Or if pb2>tau*10, nb2 is set to 10 and pb2 to 800
//Thus, the Prism set of guard/update is the following:
[to] true-> 1:(nb2'=b2<0?0:b2>800?10:floor(b2/tau)) & (pb2'=b2<0?0:b2>800?800:b2);
endmodule
