
Storm 1.6.3

Date: Fri Dec 24 02:05:45 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_STr<=10 & n_STr>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.406s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_STr <= 10) & (n_STr >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_STr <= 10) & (n_STr >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.489s.

Performance statistics:
  * peak memory usage: 435MB
  * CPU time: 33.463s
  * wallclock time: 33.907s
Storm 1.6.3

Date: Fri Dec 24 02:06:19 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_SNr<=10 & n_SNr>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 32.735s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_SNr <= 10) & (n_SNr >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_SNr <= 10) & (n_SNr >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.502s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 32.794s
  * wallclock time: 33.248s
Storm 1.6.3

Date: Fri Dec 24 02:06:53 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_GPe<=10 & n_GPe>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 35.031s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_GPe <= 10) & (n_GPe >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_GPe <= 10) & (n_GPe >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.503s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 35.178s
  * wallclock time: 35.545s
Storm 1.6.3

Date: Fri Dec 24 02:07:28 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_Delay<=10 & n_Delay>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 34.076s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_Delay <= 10) & (n_Delay >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_Delay <= 10) & (n_Delay >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.574s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 34.403s
  * wallclock time: 34.662s
Storm 1.6.3

Date: Fri Dec 24 02:08:03 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_STN<=10 & n_STN>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.567s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_STN <= 10) & (n_STN >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_STN <= 10) & (n_STN >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.507s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 33.640s
  * wallclock time: 34.086s
Storm 1.6.3

Date: Fri Dec 24 02:08:37 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (n_Th<=10 & n_Th>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.509s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_Th <= 10) & (n_Th >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((n_Th <= 10) & (n_Th >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.537s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 33.781s
  * wallclock time: 34.058s
Storm 1.6.3

Date: Fri Dec 24 02:09:11 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_STr<=800 & potentiel_STr>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.713s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_STr <= 800) & (potentiel_STr >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_STr <= 800) & (potentiel_STr >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.506s.

Performance statistics:
  * peak memory usage: 435MB
  * CPU time: 33.922s
  * wallclock time: 34.231s
Storm 1.6.3

Date: Fri Dec 24 02:09:45 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_SNr<=800 & potentiel_SNr>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.003s.

Time for model construction: 33.369s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_SNr <= 800) & (potentiel_SNr >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_SNr <= 800) & (potentiel_SNr >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.512s.

Performance statistics:
  * peak memory usage: 435MB
  * CPU time: 33.593s
  * wallclock time: 33.893s
Storm 1.6.3

Date: Fri Dec 24 02:10:19 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_GPe<=800 & potentiel_GPe>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.579s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_GPe <= 800) & (potentiel_GPe >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_GPe <= 800) & (potentiel_GPe >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.509s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 33.831s
  * wallclock time: 34.098s
Storm 1.6.3

Date: Fri Dec 24 02:10:53 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_Delay<=800 & potentiel_Delay>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.592s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_Delay <= 800) & (potentiel_Delay >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_Delay <= 800) & (potentiel_Delay >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.508s.

Performance statistics:
  * peak memory usage: 435MB
  * CPU time: 33.822s
  * wallclock time: 34.112s
Storm 1.6.3

Date: Fri Dec 24 02:11:27 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_STN<=800 & potentiel_STN>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 32.556s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_STN <= 800) & (potentiel_STN >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_STN <= 800) & (potentiel_STN >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.492s.

Performance statistics:
  * peak memory usage: 435MB
  * CPU time: 32.780s
  * wallclock time: 33.059s
Storm 1.6.3

Date: Fri Dec 24 02:12:01 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G (potentiel_Th<=800 & potentiel_Th>=0)] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.003s.

Time for model construction: 34.188s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((potentiel_Th <= 800) & (potentiel_Th >= 0)) -> 1070431 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((potentiel_Th <= 800) & (potentiel_Th >= 0))] ...
Result (for initial states): 1
Time for model checking: 0.512s.

Performance statistics:
  * peak memory usage: 436MB
  * CPU time: 34.357s
  * wallclock time: 34.712s
Storm 1.6.3

Date: Fri Dec 24 02:12:35 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_STr=0 U potentiel_STr>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 0.021s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	311
Transitions: 	590
Reward Models:  none
State Labels: 	4 labels
   * deadlock -> 0 item(s)
   * (potentiel_STr >= 80) -> 280 item(s)
   * init -> 1 item(s)
   * (n_STr = 0) -> 31 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_STr = 0) U (potentiel_STr >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.000s.

Performance statistics:
  * peak memory usage: 64MB
  * CPU time: 0.027s
  * wallclock time: 0.025s
Storm 1.6.3

Date: Fri Dec 24 02:12:35 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_SNr=0 U potentiel_SNr>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 25.369s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	813691
Transitions: 	7962670
Reward Models:  none
State Labels: 	4 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * (potentiel_SNr >= 80) -> 19360 item(s)
   * (n_SNr = 0) -> 794331 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_SNr = 0) U (potentiel_SNr >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.429s.

Performance statistics:
  * peak memory usage: 335MB
  * CPU time: 25.617s
  * wallclock time: 25.808s
Storm 1.6.3

Date: Fri Dec 24 02:13:01 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_GPe=0 U potentiel_GPe>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.003s.

Time for model construction: 21.173s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	794331
Transitions: 	6800400
Reward Models:  none
State Labels: 	4 labels
   * init -> 1 item(s)
   * (potentiel_GPe >= 80) -> 126990 item(s)
   * deadlock -> 0 item(s)
   * (n_GPe = 0) -> 667341 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_GPe = 0) U (potentiel_GPe >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.369s.

Performance statistics:
  * peak memory usage: 299MB
  * CPU time: 21.255s
  * wallclock time: 21.551s
Storm 1.6.3

Date: Fri Dec 24 02:13:23 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_Delay=0 U potentiel_Delay>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 21.259s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	794331
Transitions: 	6800400
Reward Models:  none
State Labels: 	4 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * (potentiel_Delay >= 80) -> 126990 item(s)
   * (n_Delay = 0) -> 667341 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_Delay = 0) U (potentiel_Delay >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.371s.

Performance statistics:
  * peak memory usage: 300MB
  * CPU time: 21.466s
  * wallclock time: 21.639s
Storm 1.6.3

Date: Fri Dec 24 02:13:44 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_STN=0 U potentiel_STN>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 16.243s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	667341
Transitions: 	5532750
Reward Models:  none
State Labels: 	4 labels
   * deadlock -> 0 item(s)
   * (potentiel_STN >= 80) -> 126740 item(s)
   * init -> 1 item(s)
   * (n_STN = 0) -> 540601 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_STN = 0) U (potentiel_STN >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.289s.

Performance statistics:
  * peak memory usage: 260MB
  * CPU time: 16.328s
  * wallclock time: 16.543s
Storm 1.6.3

Date: Fri Dec 24 02:14:01 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ n_Th=0 U potentiel_Th>=80 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 0.021s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	111
Transitions: 	210
Reward Models:  none
State Labels: 	4 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * (potentiel_Th >= 80) -> 100 item(s)
   * (n_Th = 0) -> 11 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [(n_Th = 0) U (potentiel_Th >= 80)] ...
Result (for initial states): 1
Time for model checking: 0.000s.

Performance statistics:
  * peak memory usage: 64MB
  * CPU time: 0.033s
  * wallclock time: 0.025s
Storm 1.6.3

Date: Fri Dec 24 02:14:01 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ F=10 n_STN>3 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.003s.

Time for model construction: 33.447s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * (n_STN > 3) -> 128960 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [true U[10, 10] (n_STN > 3)] ...
Result (for initial states): 1
Time for model checking: 2.094s.

Performance statistics:
  * peak memory usage: 936MB
  * CPU time: 35.101s
  * wallclock time: 35.555s
Storm 1.6.3

Date: Fri Dec 24 02:14:37 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ F=11 n_GPe>3&n_Delay>3 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.003s.

Time for model construction: 32.336s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_GPe > 3) & (n_Delay > 3)) -> 127590 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [true U[11, 11] ((n_GPe > 3) & (n_Delay > 3))] ...
Result (for initial states): 1
Time for model checking: 2.092s.

Performance statistics:
  * peak memory usage: 936MB
  * CPU time: 33.931s
  * wallclock time: 34.442s
Storm 1.6.3

Date: Fri Dec 24 02:15:11 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ F=12 n_STr<5&n_SNr>3 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.002s.

Time for model construction: 33.408s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1070431
Transitions: 	10704310
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 0 item(s)
   * init -> 1 item(s)
   * ((n_STr < 5) & (n_SNr > 3)) -> 19360 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [true U[12, 12] ((n_STr < 5) & (n_SNr > 3))] ...
Result (for initial states): 1
Time for model checking: 2.160s.

Performance statistics:
  * peak memory usage: 937MB
  * CPU time: 35.139s
  * wallclock time: 35.582s
Storm 1.6.3

Date: Fri Dec 24 02:15:47 2021
Command line arguments: --prism circuit.pm --prop 'P=? [ G<11 n_GPe=0&n_Delay=0 ] ' --timemem --precision 1e-12
Current working directory: /home/tlyvonne/Desktop/Benjamin_intern/Stage/model_check

Time for model input parsing: 0.004s.

ERROR (SpiritErrorHandler.h:26): Parsing error at 1:6:  expecting <path formula>, here:
	P=? [ G<11 n_GPe=0&n_Delay=0 ] 
	     ^

ERROR (storm.cpp:23): An exception caused Storm to terminate. The message of the exception is: WrongFormatException: Parsing error at 1:6:  expecting <path formula>, here:
	P=? [ G<11 n_GPe=0&n_Delay=0 ] 
	     ^

