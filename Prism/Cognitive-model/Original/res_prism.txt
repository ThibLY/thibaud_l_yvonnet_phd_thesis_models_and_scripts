
PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:15:47 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_STr<=10 & n_STr>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_STr<=10&n_STr>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_STr<=10&n_STr>=0) ]

Building model...

Computing reachable states... 53078 113793 174059 232768 295303 350725 415959 484225 552672 609612 671025 736815 804781 868166 933503 996167 1063296 1070831 states
Reachable states exploration and model construction done in 51.425 secs.
Sorting reachable states list...

Time for model construction: 52.107 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.221 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.245 seconds.

Value in the initial state: 1.0

Time for model checking: 0.357 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:16:40 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_SNr<=10 & n_SNr>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_SNr<=10&n_SNr>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_SNr<=10&n_SNr>=0) ]

Building model...

Computing reachable states... 53104 112540 172035 227353 289180 351710 409986 480551 552498 605452 667306 729936 794462 856143 918362 981959 1044627 1070831 states
Reachable states exploration and model construction done in 52.279 secs.
Sorting reachable states list...

Time for model construction: 53.021 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.229 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.25 seconds.

Value in the initial state: 1.0

Time for model checking: 0.369 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:17:34 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_GPe<=10 & n_GPe>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_GPe<=10&n_GPe>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_GPe<=10&n_GPe>=0) ]

Building model...

Computing reachable states... 54284 110978 175438 232545 297989 353863 419757 491775 558002 617241 681716 748658 814530 879422 945520 1010903 1070831 states
Reachable states exploration and model construction done in 50.697 secs.
Sorting reachable states list...

Time for model construction: 51.557 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.246 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.271 seconds.

Value in the initial state: 1.0

Time for model checking: 0.38 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:18:27 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_Delay<=10 & n_Delay>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_Delay<=10&n_Delay>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_Delay<=10&n_Delay>=0) ]

Building model...

Computing reachable states... 54039 111543 173508 228361 294467 355096 415342 487435 557347 612049 673879 736765 801493 862685 926776 991992 1057375 1070831 states
Reachable states exploration and model construction done in 51.728 secs.
Sorting reachable states list...

Time for model construction: 52.449 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.241 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.263 seconds.

Value in the initial state: 1.0

Time for model checking: 0.374 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:19:20 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_STN<=10 & n_STN>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_STN<=10&n_STN>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_STN<=10&n_STN>=0) ]

Building model...

Computing reachable states... 53279 110430 174999 229145 295273 351692 417110 489374 555968 615306 677184 740100 804407 869020 932276 998640 1066293 1070831 states
Reachable states exploration and model construction done in 51.358 secs.
Sorting reachable states list...

Time for model construction: 51.969 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.241 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.272 seconds.

Value in the initial state: 1.0

Time for model checking: 0.396 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:20:13 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (n_Th<=10 & n_Th>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (n_Th<=10&n_Th>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (n_Th<=10&n_Th>=0) ]

Building model...

Computing reachable states... 53769 112809 175478 232831 297343 350390 414105 484093 551414 610378 672470 735712 800725 861767 925346 989803 1054599 1070831 states
Reachable states exploration and model construction done in 51.883 secs.
Sorting reachable states list...

Time for model construction: 52.655 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.241 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.268 seconds.

Value in the initial state: 1.0

Time for model checking: 0.386 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:21:07 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_STr<=800 & potentiel_STr>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_STr<=800&potentiel_STr>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_STr<=800&potentiel_STr>=0) ]

Building model...

Computing reachable states... 53013 110439 175568 231023 295270 348607 412451 480608 547743 604759 662878 725162 788186 850018 912414 976326 1038672 1070831 states
Reachable states exploration and model construction done in 52.439 secs.
Sorting reachable states list...

Time for model construction: 53.132 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.48 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.506 seconds.

Value in the initial state: 1.0

Time for model checking: 0.626 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:22:01 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_SNr<=800 & potentiel_SNr>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_SNr<=800&potentiel_SNr>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_SNr<=800&potentiel_SNr>=0) ]

Building model...

Computing reachable states... 53921 109986 174358 228584 292813 345193 408752 476229 545400 602847 661975 724459 789299 849456 912420 977801 1044949 1070831 states
Reachable states exploration and model construction done in 52.207 secs.
Sorting reachable states list...

Time for model construction: 52.947 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.224 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.243 seconds.

Value in the initial state: 1.0

Time for model checking: 0.358 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:22:55 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_GPe<=800 & potentiel_GPe>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_GPe<=800&potentiel_GPe>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_GPe<=800&potentiel_GPe>=0) ]

Building model...

Computing reachable states... 53468 111651 176085 230452 296495 351819 417362 486591 553608 611104 672546 736023 802177 865995 927970 990018 1056987 1070831 states
Reachable states exploration and model construction done in 51.61 secs.
Sorting reachable states list...

Time for model construction: 52.308 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.481 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.508 seconds.

Value in the initial state: 1.0

Time for model checking: 0.627 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:23:49 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_Delay<=800 & potentiel_Delay>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_Delay<=800&potentiel_Delay>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_Delay<=800&potentiel_Delay>=0) ]

Building model...

Computing reachable states... 53226 110723 175276 228702 293656 347708 412010 483205 549808 604370 665404 728146 793115 855353 918009 979836 1044510 1070831 states
Reachable states exploration and model construction done in 52.262 secs.
Sorting reachable states list...

Time for model construction: 53.023 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.239 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.265 seconds.

Value in the initial state: 1.0

Time for model checking: 0.381 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:24:43 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_STN<=800 & potentiel_STN>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_STN<=800&potentiel_STN>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_STN<=800&potentiel_STN>=0) ]

Building model...

Computing reachable states... 54312 111095 176726 230451 295800 353224 419619 490477 555057 612203 673839 736088 800963 864364 928295 992422 1058106 1070831 states
Reachable states exploration and model construction done in 51.553 secs.
Sorting reachable states list...

Time for model construction: 52.302 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.238 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.263 seconds.

Value in the initial state: 1.0

Time for model checking: 0.374 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:25:36 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G (potentiel_Th<=800 & potentiel_Th>=0)]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G (potentiel_Th<=800&potentiel_Th>=0) ]

---------------------------------------------------------------------

Model checking: P=? [ G (potentiel_Th<=800&potentiel_Th>=0) ]

Building model...

Computing reachable states... 53765 109443 173666 230015 294494 349394 412837 479769 547294 605088 664975 729203 793243 855346 920557 986776 1051612 1070831 states
Reachable states exploration and model construction done in 51.836 secs.
Sorting reachable states list...

Time for model construction: 52.542 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.238 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 0.261 seconds.

Value in the initial state: 1.0

Time for model checking: 0.381 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:26:30 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_STr=0 U potentiel_STr>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_STr=0 U potentiel_STr>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_STr=0 U potentiel_STr>=80 ]

Building model...

Computing reachable states... 53369 109254 173750 228228 293966 347369 412942 481361 548699 605088 666664 729477 794834 857475 920852 982555 1046973 1070831 states
Reachable states exploration and model construction done in 52.076 secs.
Sorting reachable states list...

Time for model construction: 52.734 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.229 seconds)
Starting Prob0...
Prob0 took 0.319 seconds.
Starting Prob1...
Prob1 took 0.354 seconds.
target=1049200, yes=1070831, no=0, maybe=0
Probabilistic reachability took 0.924 seconds.

Value in the initial state: 1.0

Time for model checking: 1.028 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:27:24 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_SNr=0 U potentiel_SNr>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_SNr=0 U potentiel_SNr>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_SNr=0 U potentiel_SNr>=80 ]

Building model...

Computing reachable states... 53326 109186 170394 223115 287280 348522 404580 471778 545657 599518 659947 720369 785956 848487 911263 973576 1038701 1070831 states
Reachable states exploration and model construction done in 52.45 secs.
Sorting reachable states list...

Time for model construction: 53.175 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.478 seconds)
Starting Prob0...
Prob0 took 0.255 seconds.
Starting Prob1...
Prob1 took 0.283 seconds.
target=28390, yes=1070831, no=0, maybe=0
Probabilistic reachability took 1.038 seconds.

Value in the initial state: 1.0

Time for model checking: 1.147 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:28:19 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_GPe=0 U potentiel_GPe>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_GPe=0 U potentiel_GPe>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_GPe=0 U potentiel_GPe>=80 ]

Building model...

Computing reachable states... 53910 109965 173956 230042 296416 352699 417060 485397 551563 609270 668444 730852 796214 857807 921419 986539 1052971 1070831 states
Reachable states exploration and model construction done in 51.915 secs.
Sorting reachable states list...

Time for model construction: 52.512 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.222 seconds)
Starting Prob0...
Prob0 took 0.553 seconds.
Starting Prob1...
Prob1 took 0.57 seconds.
target=143490, yes=1070831, no=0, maybe=0
Probabilistic reachability took 1.366 seconds.

Value in the initial state: 1.0

Time for model checking: 1.468 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:29:14 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_Delay=0 U potentiel_Delay>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_Delay=0 U potentiel_Delay>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_Delay=0 U potentiel_Delay>=80 ]

Building model...

Computing reachable states... 53627 110244 175336 228733 293621 353367 413405 483137 553837 610552 673730 738927 804595 867156 932467 999446 1068156 1070831 states
Reachable states exploration and model construction done in 51.243 secs.
Sorting reachable states list...

Time for model construction: 51.941 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.464 seconds)
Starting Prob0...
Prob0 took 0.344 seconds.
Starting Prob1...
Prob1 took 0.313 seconds.
target=127590, yes=1070831, no=0, maybe=0
Probabilistic reachability took 1.143 seconds.

Value in the initial state: 1.0

Time for model checking: 1.249 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:30:08 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_STN=0 U potentiel_STN>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_STN=0 U potentiel_STN>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_STN=0 U potentiel_STN>=80 ]

Building model...

Computing reachable states... 53841 110559 172907 227637 293345 351403 413348 485166 553545 611272 674208 740268 804992 868882 932981 998060 1061500 1070831 states
Reachable states exploration and model construction done in 51.4 secs.
Sorting reachable states list...

Time for model construction: 52.014 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.245 seconds)
Starting Prob0...
Prob0 took 0.567 seconds.
Starting Prob1...
Prob1 took 0.53 seconds.
target=128960, yes=1070831, no=0, maybe=0
Probabilistic reachability took 1.363 seconds.

Value in the initial state: 1.0

Time for model checking: 1.468 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:31:02 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ n_Th=0 U potentiel_Th>=80 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ n_Th=0 U potentiel_Th>=80 ]

---------------------------------------------------------------------

Model checking: P=? [ n_Th=0 U potentiel_Th>=80 ]

Building model...

Computing reachable states... 53167 108529 172411 225676 289405 341039 405586 469522 541106 597867 657218 719334 784703 845541 908226 967979 1032438 1070831 states
Reachable states exploration and model construction done in 52.81 secs.
Sorting reachable states list...

Time for model construction: 53.544 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (0.226 seconds)
Starting Prob0...
Prob0 took 0.319 seconds.
Starting Prob1...
Prob1 took 0.335 seconds.
target=1062740, yes=1070831, no=0, maybe=0
Probabilistic reachability took 0.902 seconds.

Value in the initial state: 1.0

Time for model checking: 1.009 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:31:57 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [( X ( G n_STN=0 & n_GPe=0 & n_STr=0 & n_Delay=0 & n_SNr=0 & n_Th=0))]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ (X (G n_STN=0&n_GPe=0&n_STr=0&n_Delay=0&n_SNr=0&n_Th=0)) ]

---------------------------------------------------------------------

Model checking: P=? [ (X (G n_STN=0&n_GPe=0&n_STr=0&n_Delay=0&n_SNr=0&n_Th=0)) ]

Building model...

Computing reachable states... 54152 112675 179140 234870 299103 351806 417135 487334 554581 614560 676580 742747 808528 870691 934818 1001019 1068267 1070831 states
Reachable states exploration and model construction done in 51.14 secs.
Sorting reachable states list...

Time for model construction: 51.822 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Building deterministic automaton (for (X (G "L0")))...
DRA has 3 states, 1 Rabin pairs.
Time for DRA translation: 0.036 seconds.

Constructing MC-DRA product...
Time for product construction: 2.664 seconds, product has 1070831 states (1 initial), 10708310 transitions.

Finding accepting BSCCs...

Computing reachability probabilities...

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (1.095 seconds)
Starting Prob0...
Starting Prob1...
target=0, yes=0, no=1070831, maybe=0
Probabilistic reachability took 1.133 seconds.

Value in the initial state: 0.0

Time for model checking: 5.189 seconds.

Result: 0.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:32:55 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ (F n_STN>5&(X n_GPe>5&(X n_STr<5))) ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ (F n_STN>5&(X n_GPe>5&(X n_STr<5))) ]

---------------------------------------------------------------------

Model checking: P=? [ (F n_STN>5&(X n_GPe>5&(X n_STr<5))) ]

Building model...

Computing reachable states... 53574 109735 173054 226468 292959 349862 411202 481519 551748 605711 666236 729254 794190 857822 923063 989622 1057139 1070831 states
Reachable states exploration and model construction done in 51.727 secs.
Sorting reachable states list...

Time for model construction: 52.387 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Building deterministic automaton (for (F "L0"&(X "L1"&(X "L2"))))...
DRA has 7 states, 3 Rabin pairs.
Time for DRA translation: 0.037 seconds.

Constructing MC-DRA product...
Time for product construction: 5.254 seconds, product has 1814221 states (1 initial), 18142210 transitions.

Finding accepting BSCCs...

Computing reachability probabilities...

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (1.331 seconds)
Starting Prob0...
Prob0 took 0.378 seconds.
Starting Prob1...
Prob1 took 0.481 seconds.
target=1019890, yes=1814221, no=0, maybe=0
Probabilistic reachability took 2.235 seconds.

Value in the initial state: 1.0

Time for model checking: 10.169 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:33:59 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ (F n_STN>5&(X n_Delay>5&(X n_SNr>5))) ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ (F n_STN>5&(X n_Delay>5&(X n_SNr>5))) ]

---------------------------------------------------------------------

Model checking: P=? [ (F n_STN>5&(X n_Delay>5&(X n_SNr>5))) ]

Building model...

Computing reachable states... 52522 109846 175098 229632 294808 351520 416264 487094 553053 610018 668852 732010 797121 859615 920647 985643 1051822 1070831 states
Reachable states exploration and model construction done in 51.919 secs.
Sorting reachable states list...

Time for model construction: 52.619 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Building deterministic automaton (for (F "L0"&(X "L1"&(X "L2"))))...
DRA has 7 states, 3 Rabin pairs.
Time for DRA translation: 0.036 seconds.

Constructing MC-DRA product...
Time for product construction: 4.695 seconds, product has 1814221 states (1 initial), 18142210 transitions.

Finding accepting BSCCs...

Computing reachability probabilities...

Starting probabilistic reachability...
Calculating predecessor relation for discrete-time Markov chain...  done (1.269 seconds)
Starting Prob0...
Prob0 took 0.479 seconds.
Starting Prob1...
Prob1 took 0.35 seconds.
target=1019890, yes=1814221, no=0, maybe=0
Probabilistic reachability took 2.139 seconds.

Value in the initial state: 1.0

Time for model checking: 9.667 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:35:02 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ F=10 n_STN>3 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ F=10 n_STN>3 ]

---------------------------------------------------------------------

Model checking: P=? [ F=10 n_STN>3 ]

Building model...

Computing reachable states... 53434 109171 173338 225124 290927 347291 411961 484753 551980 611882 675282 737868 802949 865788 929383 993944 1056826 1070831 states
Reachable states exploration and model construction done in 51.604 secs.
Sorting reachable states list...

Time for model construction: 52.204 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Value in the initial state: 1.0

Time for model checking: 0.24 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:35:55 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ F=11 n_GPe>3&n_Delay>3 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ F=11 n_GPe>3&n_Delay>3 ]

---------------------------------------------------------------------

Model checking: P=? [ F=11 n_GPe>3&n_Delay>3 ]

Building model...

Computing reachable states... 54489 112090 176314 232686 297009 350209 414225 481457 548524 606133 665921 728213 793781 855951 917199 981765 1046595 1070831 states
Reachable states exploration and model construction done in 52.201 secs.
Sorting reachable states list...

Time for model construction: 52.798 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Value in the initial state: 1.0

Time for model checking: 0.289 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:36:48 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ F=12 n_STr<5&n_SNr>3 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ F=12 n_STr<5&n_SNr>3 ]

---------------------------------------------------------------------

Model checking: P=? [ F=12 n_STr<5&n_SNr>3 ]

Building model...

Computing reachable states... 53483 110230 174821 230739 295271 349668 414644 483154 549599 606780 666584 730210 797051 861263 922521 987258 1052415 1070831 states
Reachable states exploration and model construction done in 51.807 secs.
Sorting reachable states list...

Time for model construction: 52.538 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Value in the initial state: 1.0

Time for model checking: 0.308 seconds.

Result: 1.0 (exact floating point)

PRISM
=====

Version: 4.7
Date: Fri Dec 24 02:37:42 CET 2021
Hostname: foramen
Memory limits: cudd=15g, java(heap)=15g
Command line: prism circuit.pm -pf 'P=? [ G<11 n_GPe=0&n_Delay=0 ]' -ex -javamaxmem 15g -cuddmaxmem 15g -epsilon 1e-12 -javastack 1g -maxiters 30000

Parsing model file "circuit.pm"...

Type:        DTMC
Modules:     temps entry STr GPe STN SNr Delay Th 
Variables:   time t1 potentiel_STr n_STr potentiel_GPe n_GPe potentiel_STN n_STN potentiel_SNr n_SNr potentiel_Delay n_Delay potentiel_Th n_Th 

1 property:
(1) P=? [ G<11 n_GPe=0&n_Delay=0 ]

---------------------------------------------------------------------

Model checking: P=? [ G<11 n_GPe=0&n_Delay=0 ]

Building model...

Computing reachable states... 52713 107917 171428 223594 286573 338290 400129 466538 533417 589894 650444 710424 776602 839534 900380 961263 1024325 1070831 states
Reachable states exploration and model construction done in 53.319 secs.
Sorting reachable states list...

Time for model construction: 54.039 seconds.

Type:        DTMC
States:      1070831 (1 initial)
Transitions: 10708310

Starting bounded probabilistic reachability...
Bounded probabilistic reachability took 10 iterations and 0.179 seconds.

Value in the initial state: 1.0

Time for model checking: 0.298 seconds.

Result: 1.0 (exact floating point)

