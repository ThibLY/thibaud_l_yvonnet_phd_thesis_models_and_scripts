dtmc

module temps
time:[0..10];
[to] time<10 -> 1:(time'=time+1);
[to] time=10 -> 1:(time'=1);
endmodule

module entry
//entrée de spike suivant une loi de poisson de paramètre 7
t1:[0..3000];
[to] true -> 0.000072991952613:(t1'=1) + 0.003437086558390:(t1'=2) + 0.021604031452484:(t1'=3) + 0.059540362609726:(t1'=4) + 0.104444862957054:(t1'=5) + 0.137676978041126:(t1'=6) + 0.149002779674338:(t1'=7) + 0.139586531950597:(t1'=8) + 0.117116124452909:(t1'=9) + 0.26751825035076304:(t1'=10);
endmodule


const int tau=80;//seuil de spike
const double r=0.5;//coefficient de perte

//poids pour les neurones en bonne santé
const int fwcxstr=80;
const int fwstrgpe=40;
const int fwgpestr=80;
const int fwgpegpe=20;
const int fwstngpe=100;
const int fwgpestn=40;
const int fwstrsnr=40;
const int fwgpesnr=20;
const int fwstnDelay=80;
const int fwDelaysnr=100;
const int fwsnrth=80;
const int fwcxth=80;
const int fwstop=80;

//poids réduit
const int swcxstr=40;
const int swstrgpe=20;
const int swgpestr=40;
const int swgpegpe=10;
const int swstngpe=50;
const int swgpestn=20;
const int swstrsnr=20;
const int swgpesnr=10;
const int swstnDelay=40;
const int swDelaysnr=50;
const int swsnrth=40;
const int swcxth=40;
const int swstop=40;



//Formule du module STr
formula fSTr=floor((fwcxstr*t1)+(-fwgpestr*n_GPe)+(r*potentiel_STr*(1-(floor(potentiel_STr/tau)/10))));
formula sSTr=floor((swcxstr*t1)+(-swgpestr*n_GPe)+(r*potentiel_STr*(1-(floor(potentiel_STr/tau)/10))));

//Formule du module GPe
formula fGPe=floor((-fwstrgpe*n_STr)+(fwstngpe*n_STN)+(fwgpegpe*n_GPe)+(r*potentiel_GPe*(1-(floor(potentiel_GPe/tau)/10))));
formula sGPe=floor((-swstrgpe*n_STr)+(swstngpe*n_STN)+(swgpegpe*n_GPe)+(r*potentiel_GPe*(1-(floor(potentiel_GPe/tau)/10))));

//Formule du module STN
formula fSTN=floor((-fwgpestn*n_GPe)+(r*potentiel_STN*(1-(floor(potentiel_STN/tau)/10))));
formula sSTN=floor((-swgpestn*n_GPe)+(r*potentiel_STN*(1-(floor(potentiel_STN/tau)/10))));

//Formule du module SNr
formula fSNr=floor(-((fwstrsnr*n_STr)+(fwgpesnr*n_GPe))+(fwDelaysnr*n_Delay)+(r*potentiel_SNr*(1-(floor(potentiel_SNr/tau)/10))));
formula sSNr=floor(-((swstrsnr*n_STr)+(swgpesnr*n_GPe))+(swDelaysnr*n_Delay)+(r*potentiel_SNr*(1-(floor(potentiel_SNr/tau)/10))));

//Formule du module Delay
formula fDelay=floor((fwstnDelay*n_STN)+(r*potentiel_Delay*(1-(floor(potentiel_Delay/tau)/10))));
formula sDelay=floor((swstnDelay*n_STN)+(r*potentiel_Delay*(1-(floor(potentiel_Delay/tau)/10))));

//Formule du module Th
formula fTh=floor((-fwsnrth*n_SNr)+(fwcxth*t1)+(r*potentiel_Th*(1-(floor(potentiel_Th/tau)/10))));
formula sTh=floor((-swsnrth*n_SNr)+(swcxth*t1)+(r*potentiel_Th*(1-(floor(potentiel_Th/tau)/10))));


module STr
potentiel_STr:[0..800];
n_STr:[0..10];
[to] n_STr>=0 -> 0.8:(n_STr'=fSTr<0?0:fSTr>800?10:floor(fSTr/tau)) & (potentiel_STr'=fSTr<0?0:fSTr>800?800:fSTr) + 0.2:(n_STr'=sSTr<0?0:sSTr>800?10:floor(sSTr/tau)) & (potentiel_STr'=sSTr<0?0:sSTr>800?800:sSTr);
endmodule

module GPe
potentiel_GPe:[0..800];
n_GPe:[0..10];
[to] n_GPe>=0 -> 0.8:(n_GPe'=fGPe<0?0:fGPe>800?10:floor(fGPe/tau)) & (potentiel_GPe'=fGPe<0?0:fGPe>800?800:fGPe) + 0.2:(n_GPe'=sGPe<0?0:sGPe>800?10:floor(sGPe/tau)) & (potentiel_GPe'=sGPe<0?0:sGPe>800?800:sGPe);
endmodule

module STN
potentiel_STN:[0..800];
n_STN:[0..10];
[to] time !=9 & n_STN>=0 -> 0.8 :(n_STN'=fSTN<0?0:fSTN>800?10:floor(fSTN/tau)) & (potentiel_STN'=fSTN<0?0:fSTN>800?800:fSTN) + 0.2:(n_STN'=sSTN<0?0:sSTN>800?10:floor(sSTN/tau)) & (potentiel_STN'=sSTN<0?0:sSTN>800?800:sSTN);
//Here is another set of guard/update to add the input of the stop signal
[to] time=9 -> 1:(potentiel_STN'=800) & (n_STN'=10);
endmodule

module SNr
potentiel_SNr:[0..800];
n_SNr:[0..10];
[to] n_SNr>=0 -> 0.8:(n_SNr'=fSNr<0?0:fSNr>800?10:floor(fSNr/tau)) & (potentiel_SNr'=fSNr<0?0:fSNr>800?800:fSNr) + 0.2:(n_SNr'=sSNr<0?0:sSNr>800?10:floor(sSNr/tau)) & (potentiel_SNr'=sSNr<0?0:sSNr>800?800:sSNr);
endmodule

module Delay
potentiel_Delay:[0..800];
n_Delay:[0..10];
[to] n_Delay>=0 -> 0.8 :(n_Delay'=fDelay<0?0:fDelay>800?10:floor(fDelay/tau)) & (potentiel_Delay'=fDelay<0?0:fDelay>800?800:fDelay) + 0.2:(n_Delay'=sDelay<0?0:sDelay>800?10:floor(sDelay/tau)) & (potentiel_Delay'=sDelay<0?0:sDelay>800?800:sDelay);
endmodule

module Th
potentiel_Th:[0..800];
n_Th:[0..10];
[to] n_Th>=0 -> 1:(n_Th'=fTh<0?0:fTh>800?10:floor(fTh/tau)) & (potentiel_Th'=fTh<0?0:fTh>800?800:fTh);
endmodule
