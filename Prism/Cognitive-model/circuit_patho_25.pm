dtmc

module temps
time:[0..10];
[to] time<10 -> 1:(time'=time+1);
[to] time=10 -> 1:(time'=1);
endmodule

module entry
//entrée de spike suivant une loi de poisson de paramètre 70
t1:[0..3000];
[to] true -> 0.000072991952613:(t1'=1) + 0.003437086558390:(t1'=2) + 0.021604031452484:(t1'=3) + 0.059540362609726:(t1'=4) + 0.104444862957054:(t1'=5) + 0.137676978041126:(t1'=6) + 0.149002779674338:(t1'=7) + 0.139586531950597:(t1'=8) + 0.117116124452909:(t1'=9) + 0.26751825035076304:(t1'=10);
endmodule

const int wsncstr=30;
const int tau=80;
const double r=0.5;
const int wcxstr=80;
const int wstrgpe=40;
const int wgpestr=80;
const int wgpegpe=20;
const int wstngpe=100;
const int wgpestn=40;
const int wstrsnr=40;
const int wgpesnr=20;
const int wstnDelay=80;
const int wDelaysnr=100;
const int wsnrth=80;
const int wcxth=80;
const int wstop=80;


//Formule du module STr
formula STr_sains=floor((-wsncstr*t1)+(wcxstr*t1)+(-wgpestr*n_GPe)+(r*potentiel_STr*(1-(floor(potentiel_STr/tau)/10))));
formula STr_patho=floor((wcxstr*t1)+(-wgpestr*n_GPe)+(r*potentiel_STr*(1-(floor(potentiel_STr/tau)/10))));

//Formule du module GPe
formula GPe=floor((-wstrgpe*n_STr)+(wstngpe*n_STN)+(wgpegpe*n_GPe)+(r*potentiel_GPe*(1-(floor(potentiel_GPe/tau)/10))));

//Formule du module STN
formula STN=floor((-wgpestn*n_GPe)+(r*potentiel_STN*(1-(floor(potentiel_STN/tau)/10))));

//Formule du module SNr
formula SNr=floor(-((wstrsnr*n_STr)+(wgpesnr*n_GPe))+(wDelaysnr*n_Delay)+(r*potentiel_SNr*(1-(floor(potentiel_SNr/tau)/10))));

//Formule du module Delay
formula Delay=floor(wstnDelay*n_STN);

//Formule du module Th
formula Th=floor((-wsnrth*n_SNr)+(wcxth*t1)+(r*potentiel_Th*(1-(floor(potentiel_Th/tau)/10))));

module STr
potentiel_STr:[0..800];
n_STr:[0..10];
[to] n_STr>=0 -> 0.75:(n_STr'=STr_sains<0?0:STr_sains>800?10:floor(STr_sains/tau)) & (potentiel_STr'=STr_sains<0?0:STr_sains>800?800:STr_sains) + 0.25:(n_STr'=STr_patho<0?0:STr_patho>800?10:floor(STr_patho/tau)) & (potentiel_STr'=STr_patho<0?0:STr_patho>800?800:STr_patho);
endmodule

module GPe
potentiel_GPe:[0..800];
n_GPe:[0..10];
[to] n_GPe>=0 -> 1:(n_GPe'=GPe<0?0:GPe>800?10:floor(GPe/tau)) & (potentiel_GPe'=GPe<0?0:GPe>800?800:GPe);
endmodule

module STN
potentiel_STN:[0..800];
n_STN:[0..10];
[to] time !=9 & n_STN>=0 -> 1:(n_STN'=STN<0?0:STN>800?10:floor(STN/tau)) & (potentiel_STN'=STN<0?0:STN>800?800:STN);
//ici je rajoute la formule pour prendre en compte l'arrivé du stop signal
[to] time=9 -> 1:(potentiel_STN'=800) & (n_STN'=10);
//ici je rajoute la formule pour prendre en compte l'arrivé du stop signal
endmodule

module SNr
potentiel_SNr:[0..800];
n_SNr:[0..10];
[to] n_SNr>=0 -> 1:(n_SNr'=SNr<0?0:SNr>800?10:floor(SNr/tau)) & (potentiel_SNr'=SNr<0?0:SNr>800?800:SNr);
endmodule

module Delay
potentiel_Delay:[0..800];
n_Delay:[0..10];
[to] n_Delay>=0 -> 1:(n_Delay'=Delay<0?0:Delay>800?10:floor(Delay/tau)) & (potentiel_Delay'=Delay<0?0:Delay>800?800:Delay);
endmodule

module Th
potentiel_Th:[0..800];
n_Th:[0..10];
[to] n_Th>=0 -> 1:(n_Th'=Th<0?0:Th>800?10:floor(Th/tau)) & (potentiel_Th'=Th<0?0:Th>800?800:Th);
endmodule

