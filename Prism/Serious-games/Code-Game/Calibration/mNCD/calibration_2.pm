dtmc
// Before ruuning model checking, be sure to change in the option the model checking emgine from "Hybrid" to "Explicit".
// constants to establish the number of states
const int location_max = 2;		// Number of state for the patient (has not played yet, is playing, has played).
const int selection_max = 125;		// 125 as it is the maximum number of actions recorded for two minutes of a first trial
const int selection_min = 62;		// 62 as it is the minimum number of actions recorded for two minutes of a first trial
//const int inactivity_max = 30;		// 30 for ten seconds inactivity in five minutes=>obsolete

// formulas to determine number of state in the game
formula b=selection_min;
formula a=(-b)/(selection_max);
formula time_Over = slow=ceil(a*fast+b);		// Calculus for the timeover of the game
formula time_Is_Over = time_Over | fast>=selection_max | slow>=selection_min;		// Boolean for the timeover of the game
formula is_Time_Over = !time_Over & fast<selection_max & slow<selection_min;		// Boolean for the timeover of the game

//Computation of probability to mathc with the statistics and observations of the v=clinical experiment.
//For the 51 subject, a total of 31 errors were observed.
//
//For the miss clicks errors, two "hot spots" were observed. 7 errors around 35th action and 8 errors around 90th action. These hotspot are represented with gaussian like functions.
formula pmiss_click_1 = 1.7 * (0.0008+(1/(168*pow(2*0.3,1/2)))*pow(1.359,-pow(((fast+slow)-35)/3,2)));	//normal law centered around 35
formula pmiss_click_2 = 1.7 * (0.0008+(1/(168*pow(2*0.3,1/2)))*pow(1.359,-pow(((fast+slow)-90)/3,2)));	//normal law centered around 90

//For the anticipation error, only 1 was observed;
formula panticipate = 1.7 * (0.0002);

//For the anticipation mixed miss click errors, a "hot spot" was observed. 4 errors around 35.
formula panticipate_miss_click = 1.5 * (0.0004+(1/(330*pow(2*0.3,1/2)))*pow(1.359,-pow(((fast+slow)-35)/3,2)));	//normal law centered around 35

//For the confusion of colors or fruit errors and the double click error, only 2 were observed for each.
formula pmiss_fruit = 1.7 * (0.00041);
formula pmiss_color = 1.7 * (0.00041);
formula pdouble_click = 1.7 * (0.00041);

//For the errors of uninterpreted nature, two ""hot spots"" were observed. 2 errors around 35th action and 3 errors around 90th action.
formula punknown_miss_1 = 1.7 * (0.00022+(1/(466*pow(2*0.3,1/2)))*pow(1.359,-pow(((fast+slow)-35)/3,2)));	//normal law centered around 35
formula punknown_miss_2 = 1.7 * (0.00022+(1/(466*pow(2*0.3,1/2)))*pow(1.359,-pow(((fast+slow)-90)/3,2)));	//normal law centered around 90

//The following formula compute the probability to give a good answer.
formula psad = pmiss_click_1 + pmiss_click_2 + panticipate + panticipate_miss_click + pmiss_fruit + pmiss_color + pdouble_click + punknown_miss_1 + punknown_miss_2;
formula phapppy = 1-psad;

//For the speed of action, the mean of the mild TNC group is 96.5. The following set of probability allows to get close to that value.
const double pfast = 0.7;
formula pslow = 1 - pfast;

//As the event of left game was never observed in a game session, this action is reduced to a very low probability.
const double act_quit = 0.000000000000000000000000000000000000000000000000000001;

//Formulas the probability of each possible actions in the model. 
formula pfast_hap = (pfast * phapppy)/(1+act_quit);
formula pfast_miss_click_1 = (pfast * pmiss_click_1)/(1+act_quit);
formula pfast_miss_click_2 = (pfast * pmiss_click_2)/(1+act_quit);
formula pfast_anticipate = (pfast * panticipate)/(1+act_quit);
formula pfast_anticipate_miss_click = (pfast * panticipate_miss_click)/(1+act_quit);
formula pfast_miss_fruit = (pfast * pmiss_fruit)/(1+act_quit);
formula pfast_miss_color = (pfast * pmiss_color)/(1+act_quit);
formula pfast_double_click = (pfast * pdouble_click)/(1+act_quit);
formula pfast_unknown_miss_1 = (pfast * punknown_miss_1)/(1+act_quit);
formula pfast_unknown_miss_2 = (pfast * punknown_miss_2)/(1+act_quit);
formula pslow_hap = (pslow * phapppy)/(1+act_quit);
formula pslow_miss_click_1 = (pslow * pmiss_click_1)/(1+act_quit);
formula pslow_miss_click_2 = (pslow * pmiss_click_2)/(1+act_quit);
formula pslow_anticipate = (pslow * panticipate)/(1+act_quit);
formula pslow_anticipate_miss_click = (pslow * panticipate_miss_click)/(1+act_quit);
formula pslow_miss_fruit = (pslow * pmiss_fruit)/(1+act_quit);
formula pslow_miss_color = (pslow * pmiss_color)/(1+act_quit);
formula pslow_double_click = (pslow * pdouble_click)/(1+act_quit);
formula pslow_unknown_miss_1 = (pslow * punknown_miss_1)/(1+act_quit);
formula pslow_unknown_miss_2 = (pslow * punknown_miss_2)/(1+act_quit);
formula pquit = (act_quit)/(1+act_quit);



//An observer is added to to allow the creation of probability diagrams with run experiment tool.
module observer
	counter : [0..selection_max] init 0;
	[acts] true -> (counter'=counter+1);
endmodule

//mild TNC behaviour model
module Mild_TNC_Subject_behavior_Code_game

	location : [0..location_max] init 0;	// Subject "location" (has not played yet, is playing, has played).

	fast : [0..selection_max] init 0;	// fast answers
	slow : [0..selection_min] init 0;	// slow answers

	happy_smiley : bool init false;		// false = none, true =  good answer -> also used to attribute a reward
	sad_smiley : bool init false;		// false = none, true =  bad answer -> also used to attribute a reward
	miss_click : bool init false;		// false = none, true =  bad answer of type miss click -> also used to attribute a reward
	anticipate : bool init false;		// false = none, true =  bad answer of type anticipation -> also used to attribute a reward
	anticipate_miss_click : bool init false;// false = none, true =  bad answer of type anticipation mixed with miss click -> also used to attribute a reward
	miss_fruit : bool init false;		// false = none, true =  bad answer of type confused fruit -> also used to attribute a reward
	miss_color : bool init false;		// false = none, true =  bad answer of type confused color -> also used to attribute a reward
	double_click : bool init false;		// false = none, true =  bad answer of type double click -> also used to attribute a reward
	unknown_miss : bool init false;		// false = none, true =  bad answer of uninterpreted nature -> also used to attribute a reward
	selection : bool init false;		// false = does not answer, true gives an answer -> used to attribut a reward each time there is a selection
	quit_game : bool init false;		// false = stay, true = leave game -> used to attribut a reward each time there is a selection

	// the patient presses the start button
	[pressStart] location=0 -> (location'=1); 

	//The subject plays and make one of their possible actions
	[acts] location=1 & is_Time_Over & quit_game=false -> pfast_hap : (fast'=fast+1) & (happy_smiley'=true) & (sad_smiley'=false) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_miss_click_1 : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=true) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_miss_click_2 : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=true) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_anticipate : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=true) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_anticipate_miss_click : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=true) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_miss_fruit : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=true) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pfast_miss_color : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=true) & (double_click'=false) & (unknown_miss'=false) + pfast_double_click : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=true) & (unknown_miss'=false) + pfast_unknown_miss_1 : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=true) + pfast_unknown_miss_2 : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=true) + pslow_hap : (slow'=slow+1) & (happy_smiley'=true) & (sad_smiley'=false) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_miss_click_1 : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=true) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_miss_click_2 : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=true) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_anticipate : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=true) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_anticipate_miss_click : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=true) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_miss_fruit : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=true) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=false) + pslow_miss_color : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=true) & (double_click'=false) & (unknown_miss'=false) + pslow_double_click : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=true) & (unknown_miss'=false) + pslow_unknown_miss_1 : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=true) + pslow_unknown_miss_2 : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) & (miss_click'=false) & (anticipate'=false) & (anticipate_miss_click'=false) & (miss_fruit'=false) & (miss_color'=false) & (double_click'=false) & (unknown_miss'=true) + pquit : (quit_game'=true) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=true);

	// the game is over as 2minutes elapsed
	[timeOver] location=1 & time_Is_Over -> (location'=location+1) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=false);

	// the game ends as the subject quitted before the end of the two minutes
	[leftGame] location=1 & quit_game=true -> (location'=location+1) & (quit_game'=false) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=false);

	//ending the model, needed to avoid deadlocks during model-checking
	[end] location=2 -> true;

endmodule


//Reward

rewards "Happy_smiley_reward"
	happy_smiley=true : 1;
endrewards

rewards "Sad_smiley_reward"
	sad_smiley=true : 1;
endrewards

rewards "miss_click_rew"
	miss_click=true : 1;
endrewards

rewards "anticipate_rew"
	anticipate=true : 1;
endrewards

rewards "anticipate_miss_click_rew"
	anticipate_miss_click=true : 1;
endrewards

rewards "miss_fruit_rew"
	miss_fruit=true : 1;
endrewards

rewards "miss_color_rew"
	miss_color=true : 1;
endrewards

rewards "double_click_rew"
	double_click=true : 1;
endrewards

rewards "unknown_miss_rew"
	unknown_miss=true : 1;
endrewards

rewards "Num_selection"
	selection=true : 1;
endrewards

rewards "Leave_game_reward"
	quit_game=true : 1;
endrewards

//obsolete
//rewards "Gaming_time"
//	happy_smiley=true : 3;
//	sad_smiley=true : 3;
//	non_interaction=true : 10;
//endrewards
