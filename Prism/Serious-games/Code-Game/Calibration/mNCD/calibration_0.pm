dtmc
// Before ruuning model checking, be sure to change in the option the model checking emgine from "Hybrid" to "Sparse".
// constants to establish the number of states
const int location_max = 2;			// (number of state for the patient)
const int selection_max = 125;		// (125 as it is the maximum number of actions recorded for two minutes of a first trial)
//const int inactivity_max = 30;		// (30 for ten seconds inactivity in five minutes)
const int selection_min = 62;		// (62 as it is the minimum number of actions recorded for two minutes of a first trial)

// formulas to determine number of state in the game
formula b=selection_min;
formula a=(-b)/(selection_max);
formula time_Over = slow=ceil(a*fast+b);		// Calculus for the timeover of the game
formula time_Is_Over = time_Over | fast>=selection_max | slow>=selection_min;		// Calculus for the timeover of the game
formula is_Time_Over = !time_Over & fast<selection_max & slow<selection_min;		// Calculus for the timeover of the game


// constant for the weight given by the doctor to determine the probability of each arc for action one
const double act_one_weight_one = 0.5;
const double act_one_weight_two = 0.25;
const double act_one_weight_three = 0.25;
const double act_one_weight_four = 0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001;

const double act_fast = 0.7;
const double act_slow = 0.3;
const double act_happy = 0.99;
const double act_sad = 0.01;
const double act_quit = 0.0000000000000000001;



//in sad : miss click starts high on 30 then uniform until 85 to 105



// formula for the sum of the previous weight
formula sum_weight_one = act_one_weight_one + act_one_weight_two + act_one_weight_three + act_one_weight_four;

// formulas to determine the probability for each arc
formula p1 = (act_one_weight_one/sum_weight_one) - 0.35*((fast+slow*10/3)/(150));
formula p2 = (act_one_weight_two/sum_weight_one) + 0.21*((fast+slow*10/3)/(150));
formula p3 = (act_one_weight_three/sum_weight_one) + 0.09*((fast+slow*10/3)/(150));
formula p4 = (act_one_weight_four/sum_weight_one) + 0.05*((fast+slow*10/3)/(150));


formula pfast_hap = (act_fast * act_happy)/(1+act_quit);
formula pfast_sad = (act_fast * act_sad)/(1+act_quit);
formula pslow_hap = (act_slow * act_happy)/(1+act_quit);
formula pslow_sad = (act_slow * act_sad)/(1+act_quit);
formula pquit = (act_quit)/(1+act_quit);

//Markovian chain

module Serious_game

	location : [0..location_max] init 0;		// state of the patient

	fast : [0..selection_max] init 0;	// interactions with the game
	slow : [0..selection_min] init 0;	// non-interactions with the game

	happy_smiley : bool init false;		// false = none, true =  good answer -> also used to attribute a reward
	sad_smiley : bool init false;		// false = none, true =  bad answer -> also used to attribute a reward
//	non_interaction : bool init false;	// false = interaction, true = non-interaction -> used to attribute a reward each time there is a non-interaction
	selection : bool init false;		// false = does not select, true make a selection -> used to attribut a reward each time there is a selection
	quit_game : bool init false;		// false = stay, true = leave game -> shall we use reward on it???


	[pressStart] location=0 -> (location'=1); // the patient presses the start button

	[acts] location=1 & is_Time_Over & quit_game=false -> pfast_hap : (fast'=fast+1) & (happy_smiley'=true) & (sad_smiley'=false) & (selection'=true) + pfast_sad : (fast'=fast+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) + pslow_hap : (slow'=slow+1) & (happy_smiley'=true) & (sad_smiley'=false) & (selection'=true) + pslow_sad : (slow'=slow+1) & (happy_smiley'=false) & (sad_smiley'=true) & (selection'=true) + pquit : (quit_game'=true) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=true); //while loop

	[timeOver] location=1 & time_Is_Over -> (location'=location+1) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=false); // the game is over

	[leftGame] location=1 & quit_game=true -> (location'=location+1) & (quit_game'=false) & (happy_smiley'=false) & (sad_smiley'=false) & (selection'=false); // the game ends

	[] location=2 -> true; //ending the model, needed to avoid deadlocks during model-checking

endmodule



//Reward

rewards "Happy_smiley_reward"
	happy_smiley=true : 1;
endrewards

rewards "Sad_smiley_reward"
	sad_smiley=true : 1;
endrewards

//rewards "Non_interaction_reward"
//	non_interaction=true : 1;
//endrewards

rewards "Num_selection"
	selection=true : 1;
endrewards

rewards "Leave_game_reward"
	quit_game=true : 1;
endrewards

//rewards "Gaming_time"
//	happy_smiley=true : 3;
//	sad_smiley=true : 3;
//	non_interaction=true : 10;
//endrewards
