dtmc


// quantity of original pictures not doubled
const int easy_original_nd_image_max = 18;
const int med_original_nd_image_max = 8;
const int diff_original_nd_image_max = 2;
// quantity of original pictures which will be doubled
const int med_original_d_image_max = 4;
const int diff_original_d_image_max = 2;
// total quantity of original pictures
const int original_image_max = easy_original_nd_image_max+med_original_nd_image_max+diff_original_nd_image_max+med_original_d_image_max+diff_original_d_image_max;
// quantity of doubled pictures
const int med_doubled_image_max = 4;
const int diff_doubled_image_max = 2;
// total quantity of doubled pictures
const int double_image_max = med_doubled_image_max+diff_doubled_image_max;

//total quantity of images
const int nb_pictures_max = original_image_max+double_image_max;


module Serious_game

	game_on : bool init false;

	nb_pictures : [0..nb_pictures_max] init 0;

	//Start game
	[pressStart] game_on=false -> (game_on'=true);

	//Transition to nb_easy_original_nd
	[easy_folder] game_on=true & (nb_pictures<=2 | nb_pictures=4 | (nb_pictures>=6 & nb_pictures<=7) | nb_pictures=10 | nb_pictures=12 | (nb_pictures>=14 & nb_pictures<=15) | (nb_pictures>=25 & nb_pictures<=26) | nb_pictures=29 | (nb_pictures>=33 & nb_pictures<=35) | (nb_pictures>=37 & nb_pictures<=38)) -> (nb_pictures'=nb_pictures+1);
	//Transition to nb_med_original_nd
	[med_folder] game_on=true & (nb_pictures=5 | nb_pictures=17 | (nb_pictures>=20 & nb_pictures<=22) | nb_pictures=27 | nb_pictures=31 | nb_pictures=39) -> (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_nd
	[diff_folder] game_on=true & (nb_pictures=8 | nb_pictures=32) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d (which is associated with a double)
	[med_folder] game_on=true & (nb_pictures=9 | nb_pictures=13 | nb_pictures=18 | nb_pictures=28) -> (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_d (which is associated with a double)
	[diff_folder] game_on=true & (nb_pictures=3| nb_pictures=23) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d
	[med_bin] game_on=true & (nb_pictures=16 | nb_pictures=19 | nb_pictures=24 | nb_pictures=36) -> (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_d
	[diff_bin] game_on=true & (nb_pictures=11| nb_pictures=30) -> (nb_pictures'=nb_pictures+1);

	//End game
	[EndGame] game_on=true & nb_pictures=nb_pictures_max -> (game_on'=false);

	[EndLoop] game_on=false & nb_pictures=nb_pictures_max -> true;

endmodule


// Setting probabilities
// Probability for easy original pictures
formula p_easy_folder_f = 0.90 ;
formula p_easy_folder_b = 0.10;
formula p_easy_folder_f_tot = p_easy_folder_f;
formula p_easy_folder_b_tot = p_easy_folder_b;

// Probability for medium original pictures
formula p_med_folder_f = 0.80;
formula p_med_folder_b = 0.20;
formula p_med_folder_f_tot = p_easy_folder_f;
formula p_med_folder_b_tot = p_easy_folder_b;
// Probability for difficult original pictures
formula p_diff_folder_f = 0.60;
formula p_diff_folder_b = 0.40;
formula p_diff_folder_f_tot = p_easy_folder_f;
formula p_diff_folder_b_tot = p_easy_folder_b;

// Probability for medium double pictures
formula p_med_bin_f = 0.30;
formula p_med_bin_b = 0.70;
formula p_med_bin_f_tot = p_easy_folder_f;
formula p_med_bin_b_tot = p_easy_folder_b;

// Probability for easy double pictures
formula p_diff_bin_f = 0.50;
formula p_diff_bin_b = 0.50;
formula p_diff_bin_f_tot = p_easy_folder_f;
formula p_diff_bin_b_tot = p_easy_folder_b;

module patient
	will_play : bool init true;
	in_room : bool init false;
	choose_folder : bool init false;
	choose_bin : bool init false;



	//Start game
	[pressStart] true -> (in_room'=true);

	//Transition to nb_easy_original_nd
	[easy_folder] true -> p_easy_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);

	//Transition to nb_med_original_nd
	[med_folder] true -> p_med_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);

	//Transition to nb_diff_original_nd
	[diff_folder] true-> p_diff_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);

	//Transition to nb_med_original_d
	[med_bin] true -> p_med_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);

	//Transition to nb_diff_original_d
	[diff_bin] true -> p_diff_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);

	//End game
	[EndGame] will_play=true & in_room=true -> (will_play'=false) & (in_room'=false) & (choose_folder' = false) & (choose_bin' = false);

	//Boucle final
	[EndLoop] true -> true;

endmodule



rewards "total_answered"
	choose_folder : 1;
	choose_bin : 1;
endrewards

rewards "total_answered_trans"
	[easy_folder] true : 1;
	[med_folder] true : 1;
	[diff_folder] true : 1;
	[med_bin] true : 1;
	[diff_bin] true : 1;
endrewards

rewards "total_hesitate"
	choose_folder : 1;
	choose_bin : 1;
endrewards
