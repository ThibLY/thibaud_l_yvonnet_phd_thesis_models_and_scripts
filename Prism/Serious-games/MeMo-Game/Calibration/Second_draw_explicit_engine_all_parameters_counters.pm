dtmc

// constants to establish the number of states
const int patient_state_max = 2;			// (number of state for the patient)


// quantity of unique pictures not doubled
const int easy_unique_image_max = 18;
const int med_unique_image_max = 8;
const int diff_unique_image_max = 2;
// quantity of original pictures which will be doubled
const int med_original_image_max = 4;
const int diff_original_image_max = 2;
// total quantity of original pictures
const int original_image_max = easy_unique_image_max+med_unique_image_max+diff_unique_image_max+med_original_image_max+diff_original_image_max;
// quantity of doubled pictures
const int med_doubled_image_max = 4;
const int diff_doubled_image_max = 2;
// total quantity of doubled pictures
const int double_image_max = med_doubled_image_max+diff_doubled_image_max;

//total quantity of images
const int nb_pictures_max = original_image_max+double_image_max;


module game

	game_on : bool init false;

	nb_pictures : [0..nb_pictures_max] init 0;


	//Start game
	[pressStart] game_on=false -> (game_on'=true);

	//Transition to nb_easy_original_nd
	[easyFolder] game_on=true & (nb_pictures<=2 | nb_pictures=4 | (nb_pictures>=6 & nb_pictures<=7) | nb_pictures=10 | nb_pictures=12 | (nb_pictures>=14 & nb_pictures<=15) | (nb_pictures>=25 & nb_pictures<=26) | nb_pictures=29 | (nb_pictures>=33 & nb_pictures<=35) | (nb_pictures>=37 & nb_pictures<=38)) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_nd
	[medFolder] game_on=true & (nb_pictures=5 | nb_pictures=17 | (nb_pictures>=20 & nb_pictures<=22) | nb_pictures=27 | nb_pictures=31 | nb_pictures=39) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_diff_original_nd
	[diffFolder] game_on=true & (nb_pictures=8 | nb_pictures=32) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d (which is associated with a double)
	[medFolder] game_on=true & (nb_pictures=9 | nb_pictures=13 | nb_pictures=18 | nb_pictures=28) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_diff_original_d (which is associated with a double)
	[diffFolder] game_on=true & (nb_pictures=3| nb_pictures=23) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d
	[medBin] game_on=true & (nb_pictures=16 | nb_pictures=19 | nb_pictures=24 | nb_pictures=36) -> (nb_pictures'=nb_pictures+1);

	//Transition to nb_diff_original_d
	[diffBin] game_on=true & (nb_pictures=11| nb_pictures=30) -> (nb_pictures'=nb_pictures+1);

	//End game
	[endGame] game_on=true & nb_pictures=nb_pictures_max -> (game_on'=false);

	[endLoop] game_on=false & nb_pictures=nb_pictures_max -> true;

endmodule


// Setting probabilities
// Probability for easy original pictures
formula p_easy_folder_f = 0.90-(0.10*counter/240);
formula p_easy_folder_b = 0.10+(0.10*counter/240);
formula p_easy_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_easy_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for medium original pictures
formula p_med_folder_f = 0.80-(0.15*counter/240);
formula p_med_folder_b = 0.20+(0.15*counter/240);
formula p_med_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_med_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for difficult original pictures
formula p_diff_folder_f = 0.60-(0.18*counter/240);
formula p_diff_folder_b = 0.40+(0.18*counter/240);
formula p_diff_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_diff_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for medium double pictures
formula p_med_bin_f = 0.60-(0.15*counter/240);
formula p_med_bin_b = 0.40+(0.15*counter/240);
formula p_med_bin_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_med_bin_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for easy double pictures
formula p_diff_bin_f = 0.50-(0.20*counter/240);
formula p_diff_bin_b = 0.50+(0.20*counter/240);
formula p_diff_bin_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_diff_bin_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for hesitation
formula p_start_hesi = 0.10+(0.10*counter/240);
formula p_cont_hesi = 0.20+(0.20*(counter/240))+(0.10*(hesi_count/5));
formula p_stop_hesi = 0.80-(0.20*(counter/240))-(0.10*(hesi_count/5));

//Probability to quit the game
formula p_quit_game = 0.0002;


module patient
	will_play : bool init true;
	front_screen : bool init false;
	choose_folder : bool init false;
	choose_bin : bool init false;
	quit_game : bool init false;
	hesitate : bool init false;
	hesi_count : [0..5] init 0;
	good_easy_uni : [0..18];
	good_diff_uni_ori : [0..4];


	//the next variables are used to allow the module to give an answer after hesitating
	easy_fold: bool init false;
	med_fold : bool init false;
	diff_fold : bool init false;
	med_bin : bool init false;
	diff_bin : bool init false;


	//Start game
	[pressStart] will_play=true & front_screen=false -> (front_screen'=true);

	//Transition to nb_easy_original_nd
	[easyFolder] will_play=true & front_screen=true & quit_game=false & hesitate=false -> p_easy_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) & (good_easy_uni'=good_easy_uni+1) + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (easy_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_med_original_nd
	[medFolder] will_play=true & front_screen=true & quit_game=false & hesitate=false -> p_med_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_nd
	[diffFolder] will_play=true & front_screen=true & quit_game=false & hesitate=false -> p_diff_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) & (good_diff_uni_ori'=good_diff_uni_ori+1) + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//Transition to nb_med_original_d
	[medBin] will_play=true & front_screen=true & quit_game=false & hesitate=false -> p_med_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_d
	[diffBin] will_play=true & front_screen=true & quit_game=false & hesitate=false ->  p_diff_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//Hesitation transition
	//Continue hesitation or not on an easy_orig/uni
	[patientHesitate] hesitate=true & hesi_count<5 & easy_fold=true -> p_stop_hesi*p_easy_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) & (good_easy_uni'=good_easy_uni+1)+ p_stop_hesi*p_easy_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	[patientHesitate] hesitate=true & hesi_count=5 & easy_fold=true -> p_easy_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) & (good_easy_uni'=good_easy_uni+1) + p_easy_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not on a medium orig/uni
	[patientHesitate] hesitate=true & hesi_count<5 & med_fold=true -> p_stop_hesi*p_med_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_med_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	[patientHesitate] hesitate=true & hesi_count=5 & med_fold=true -> p_med_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_med_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not on a diff ori/uni
	[patientHesitate] hesitate=true & hesi_count<5 & diff_fold=true -> p_stop_hesi*p_diff_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) & (good_diff_uni_ori'=good_diff_uni_ori+1) + p_stop_hesi*p_diff_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	[patientHesitate] hesitate=true & hesi_count=5 & diff_fold=true -> p_diff_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) & (good_diff_uni_ori'=good_diff_uni_ori+1) + p_diff_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not on a medium dupl
	[patientHesitate] hesitate=true & hesi_count<5 & med_bin=true -> p_stop_hesi*p_med_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_med_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	[patientHesitate] hesitate=true & hesi_count=5 & med_bin=true -> p_med_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_med_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not on a diff dupl
	[patientHesitate] hesitate=true & hesi_count<5 & diff_bin=true -> p_stop_hesi*p_diff_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_diff_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	[patientHesitate] hesitate=true & hesi_count=5 & diff_bin=true -> p_diff_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_diff_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);


	[quitGame] quit_game=true -> (will_play' = false) & (front_screen'=false);


	//End game
	[endGame] will_play=true & front_screen=true & hesitate=false -> (will_play'=false) & (front_screen'=false) & (choose_folder' = false) & (choose_bin' = false);

	//Boucle final
	[endLoop] will_play=false & front_screen=false -> true;


endmodule


//facteur de fatigue, diminuer la probabilité des bonnes réponses. coefficient différent voir calcul différent pour témoin et patient.
//facteur de fatigue, augmenter le temps de réponse donc d'hésitation sur les images

module observer

	counter : [0..240] init 0;



	//Transition to nb_easy_original_nd
	[easyFolder] true -> (counter'=counter+1);
	//Transition to nb_med_original_nd
	[medFolder] true -> (counter'=counter+1);
	//Transition to nb_diff_original_nd
	[diffFolder] true -> (counter'=counter+1);

	//Transition to nb_med_original_d
	[medBin] true -> (counter'=counter+1);
	//Transition to nb_diff_original_d
	[diffBin] true -> (counter'=counter+1);

	//Hesitation transition
	[patientHesitate] true -> (counter'=counter+1);

endmodule

// Quelle est la probabilité de mettre 40 folder.
// Quelle est la probabilité de mettre 12 bin.
// Quelle est la probabilité de répondre plus juste sur les simples uniques que sur les complexes unique. Faire normalisation

//rewards "total_answered"
//	choose_folder : 1;
//	choose_bin : 1;
//endrewards

//rewards "good_answered"
//	choose_folder : 1;
//	choose_bin : 1;
//endrewards

//rewards "wrong_answered"
//	choose_folder : 1;
//	choose_bin : 1;
//endrewards

//rewards "total_hesitate"
//	hesitate : 1;
//endrewards

//rewards "quit"
//	hesitate : 1;
//endrewards

//rewards "chose_folder"
//	choose_folder : 1;
//endrewards

//rewards "chose_bin"
//	choose_bin : 1;
//endrewards