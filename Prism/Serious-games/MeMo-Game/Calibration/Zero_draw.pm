dtmc

// constants to establish the number of states
const int patient_state_max = 2;			// (number of state for the patient)


// quantity of original pictures not doubled
const int easy_original_nd_image_max = 18;
const int med_original_nd_image_max = 8;
const int diff_original_nd_image_max = 2;
// quantity of original pictures which will be doubled
const int med_original_d_image_max = 4;
const int diff_original_d_image_max = 2;
// total quantity of original pictures
const int original_image_max = easy_original_nd_image_max+med_original_nd_image_max+diff_original_nd_image_max+med_original_d_image_max+diff_original_d_image_max;
// quantity of doubled pictures
const int med_doubled_image_max = 4;
const int diff_doubled_image_max = 2;
// total quantity of doubled pictures
const int double_image_max = med_doubled_image_max+diff_doubled_image_max;

//total quantity of images
const int nb_pictures_max = original_image_max+double_image_max;


module Serious_game

	game_on : bool init false;
	
//	is_original : bool init false;
//	is_double : bool init false;

	nb_pictures : [0..nb_pictures_max] init 0;
//	nb_easy_original_nd : [0..easy_original_nd_image_max] init 0;
//	nb_med_original_nd : [0..med_original_nd_image_max] init 0;
//	nb_diff_original_nd : [0..diff_original_nd_image_max] init 0;
//	nb_med_original_d : [0..med_original_d_image_max] init 0;
//	nb_diff_original_d : [0..diff_original_d_image_max] init 0;
//	nb_med_doubled : [0..med_doubled_image_max] init 0;
//	nb_diff_doubled : [0..diff_doubled_image_max] init 0;

	//Start game
	[pressStart] game_on=false -> (game_on'=true);

	//Transition to nb_easy_original_nd
	[easy_folder] game_on=true & (nb_pictures<=2 | nb_pictures=4 | (nb_pictures>=6 & nb_pictures<=7) | nb_pictures=10 | nb_pictures=12 | (nb_pictures>=14 & nb_pictures<=15) | (nb_pictures>=25 & nb_pictures<=26) | nb_pictures=29 | (nb_pictures>=33 & nb_pictures<=35) | (nb_pictures>=37 & nb_pictures<=38)) -> (nb_pictures'=nb_pictures+1);// (nb_easy_original_nd'=nb_easy_original_nd+1) & (nb_pictures'=nb_pictures+1);
	//Transition to nb_med_original_nd
	[med_folder] game_on=true & (nb_pictures=5 | nb_pictures=17 | (nb_pictures>=20 & nb_pictures<=22) | nb_pictures=27 | nb_pictures=31 | nb_pictures=39) -> (nb_pictures'=nb_pictures+1);// (nb_med_original_nd'=nb_med_original_nd+1) & (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_nd
	[diff_folder] game_on=true & (nb_pictures=8 | nb_pictures=32) -> (nb_pictures'=nb_pictures+1);// (nb_diff_original_nd'=nb_diff_original_nd+1) & (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d (which is associated with a double)
	[med_folder] game_on=true & (nb_pictures=9 | nb_pictures=13 | nb_pictures=18 | nb_pictures=28) -> (nb_pictures'=nb_pictures+1);// (nb_med_original_d'=nb_med_original_d+1) & (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_d (which is associated with a double)
	[diff_folder] game_on=true & (nb_pictures=3| nb_pictures=23) -> (nb_pictures'=nb_pictures+1);// (nb_diff_original_d'=nb_diff_original_d+1) & (nb_pictures'=nb_pictures+1);

	//Transition to nb_med_original_d
	[med_bin] game_on=true & (nb_pictures=16 | nb_pictures=19 | nb_pictures=24 | nb_pictures=36) -> (nb_pictures'=nb_pictures+1);// (nb_med_doubled'=nb_med_doubled+1) & (nb_pictures'=nb_pictures+1);
	//Transition to nb_diff_original_d
	[diff_bin] game_on=true & (nb_pictures=11| nb_pictures=30) -> (nb_pictures'=nb_pictures+1);// (nb_diff_doubled'=nb_diff_doubled+1) & (nb_pictures'=nb_pictures+1);

	//End game
	[EndGame] game_on=true & nb_pictures=nb_pictures_max -> (game_on'=false);

	[EndLoop] game_on=false & nb_pictures=nb_pictures_max -> true;

endmodule


// Setting probabilities
// Probability for easy original pictures
formula p_easy_folder_f = 0.90 ;
formula p_easy_folder_b = 0.10;
formula p_easy_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_easy_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for medium original pictures
formula p_med_folder_f = 0.80;
formula p_med_folder_b = 0.20;
formula p_med_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_med_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for difficult original pictures
formula p_diff_folder_f = 0.60;
formula p_diff_folder_b = 0.40;
formula p_diff_folder_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_diff_folder_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for medium double pictures
formula p_med_bin_f = 0.30;
formula p_med_bin_b = 0.70;
formula p_med_bin_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_med_bin_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for easy double pictures
formula p_diff_bin_f = 0.50;
formula p_diff_bin_b = 0.50;
formula p_diff_bin_f_tot = p_easy_folder_f * (1-(p_start_hesi + p_quit_game));
formula p_diff_bin_b_tot = p_easy_folder_b * (1-(p_start_hesi + p_quit_game));

// Probability for hesitation
formula p_start_hesi = 0.00;
//formula p_cont_hesi = 0.20;
//formula p_stop_hesi = 0.80;

//Probability to quit the game
formula p_quit_game = 0.00;


module patient
	will_play : bool init true;
	in_room : bool init false;
	choose_folder : bool init false;
	choose_bin : bool init false;
//	quit_game : bool init false;
//	hesitate : bool init false;
//	hesi_count : [0..5] init 0;
//	end_game : bool init false;
//	action : [0..100] init 0;
	//the next variables are used to allow the module to give an answer after hesitating
//	easy_fold: bool init false;
//	med_fold : bool init false;
//	diff_fold : bool init false;
//	med_bin : bool init false;
//	diff_bin : bool init false;


	//Start game
	//[pressStart] will_play=true & in_room=false -> (in_room'=true);

	//Transition to nb_easy_original_nd
	//[easy_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_easy_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (easy_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_med_original_nd
	//[med_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_med_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_nd
	//[diff_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_diff_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//Transition to nb_med_original_d
	//[med_bin] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_med_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_d
	//[diff_bin] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_diff_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true) + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//Hesitation transition
	//Continue hesitation or not on an easy_original
	//[hesitate] hesitate=true & hesi_count<5 & easy_fold=true -> p_stop_hesi*p_easy_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_easy_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	//[hesitate] hesitate=true & hesi_count=5 & easy_fold=true -> p_easy_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_easy_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not
	//[hesitate] hesitate=true & hesi_count<5 & med_fold=true -> p_stop_hesi*p_med_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_med_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	//[hesitate] hesitate=true & hesi_count=5 & med_fold=true -> p_med_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_med_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not
	//[hesitate] hesitate=true & hesi_count<5 & diff_fold=true -> p_stop_hesi*p_diff_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_diff_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	//[hesitate] hesitate=true & hesi_count=5 & diff_fold=true -> p_diff_folder_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_diff_folder_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not
	//[hesitate] hesitate=true & hesi_count<5 & med_bin=true -> p_stop_hesi*p_med_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_med_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	//[hesitate] hesitate=true & hesi_count=5 & med_bin=true -> p_med_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_med_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);

	//Continue hesitation or not
	//[hesitate] hesitate=true & hesi_count<5 & diff_bin=true -> p_stop_hesi*p_diff_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_stop_hesi*p_diff_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true) + p_cont_hesi : (hesi_count'=hesi_count+1);
	//Stop hesitation
	//[hesitate] hesitate=true & hesi_count=5 & diff_bin=true -> p_diff_bin_f : (hesitate'=false) & (hesi_count'=0) & (choose_folder' = true) + p_diff_bin_b : (hesitate'=false) & (hesi_count'=0) & (choose_bin' = true);


	//[QuitGame] quit_game=true -> (will_play' = false) & (in_room'=false) & (choose_folder' = false) & (choose_bin' = false);


	//End game
	//[EndGame] will_play=true & in_room=true -> (will_play'=false) & (in_room'=false) & (choose_folder' = false) & (choose_bin' = false);

	//Boucle final
	//[EndLoop] will_play=false & in_room=false -> true;

	//Start game
//	[pressStart] will_play=true & in_room=false -> (in_room'=true);
	[pressStart] true -> (in_room'=true);

	//Transition to nb_easy_original_nd
//	[easy_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_easy_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (easy_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
//	[easy_folder] true -> p_easy_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (easy_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	[easy_folder] true -> 0.95 : (choose_folder' = true) & (choose_bin' = false) + 0.05 : (choose_folder' = false) & (choose_bin' = false);// + p_easy_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (easy_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_med_original_nd
//	[med_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_med_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
//	[med_folder] true -> p_med_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	[med_folder] true -> 0.5 : (choose_folder' = true) & (choose_bin' = false) + 0.5 : (choose_folder' = false) & (choose_bin' = true);// + p_med_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_nd
//	[diff_folder] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_diff_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
//	[diff_folder] true-> p_diff_folder_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	[diff_folder] true-> 0.5 : (choose_folder' = true) & (choose_bin' = false) + 0.5 : (choose_folder' = false) & (choose_bin' = true);// + p_diff_folder_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_fold'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//Transition to nb_med_original_d
//	[med_bin] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_med_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
//	[med_bin] true -> p_med_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	[med_bin] true -> 0.5 : (choose_folder' = true) & (choose_bin' = false) + 0.5 : (choose_folder' = false) & (choose_bin' = true);// + p_med_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (med_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	//Transition to nb_diff_original_d
//	[diff_bin] will_play=true & in_room=true & quit_game=false & hesitate=false -> p_diff_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
//	[diff_bin] true -> p_diff_bin_f_tot : (choose_folder' = true) & (choose_bin' = false) + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);
	[diff_bin] true -> 0.5 : (choose_folder' = true) & (choose_bin' = false) + 0.5 : (choose_folder' = false) & (choose_bin' = true);// + p_diff_bin_b_tot : (choose_folder' = false) & (choose_bin' = true);// + p_start_hesi : (hesitate'=true) & (hesi_count'=hesi_count+1) & (diff_bin'=true) & (choose_folder' = false) & (choose_bin' = false) + p_quit_game : (quit_game'=true) & (choose_folder' = false) & (choose_bin' = false);

	//End game
//	[EndGame] will_play=true & in_room=true & quit_game=false & hesitate=false -> (will_play'=false) & (in_room'=false) & (choose_folder' = false) & (choose_bin' = false);
	[EndGame] true -> (will_play'=false) & (in_room'=false) & (choose_folder' = false) & (choose_bin' = false);

	//Boucle final
//	[EndLoop] will_play=false & in_room=false -> true;
	[EndLoop] true -> true;

endmodule


//facteur de fatigue, diminuer la probabilité des bonnes réponses. coefficient différent voir calcul différent pour témoin et patient.

//facteur de fatigue, augmenter le temps de réponse donc d'hésitation sur les images
//module observator
//	counter1 : [0..300] init 0;
//	counter2 : [0..300] init 0;

	//Start game
//	[pressStart] true -> (counter1'=counter1+1);

	//Transition to nb_easy_original_nd
//	[easy_folder] true -> (counter1'=counter1+1) & (counter2'=counter2+1);
	//Transition to nb_med_original_nd
//	[med_folder] true -> (counter1'=counter1+1) & (counter2'=counter2+1);
	//Transition to nb_diff_original_nd
//	[diff_folder] true -> (counter1'=counter1+1) & (counter2'=counter2+1);

	//Transition to nb_med_original_d
//	[med_bin] true -> (counter1'=counter1+1) & (counter2'=counter2+1);
	//Transition to nb_diff_original_d
//	[diff_bin] true -> (counter1'=counter1+1) & (counter2'=counter2+1);

	//Hesitation transition
	//Continue hesitation or not on an easy_original
	//[hesitate] true -> (counter'=counter+1);

	//[QuitGame] true -> (counter'=counter+1);


	//End game
//	[EndGame] true -> (counter1'=counter1+1);

	//Boucle final
//	[EndLoop] true -> true;

//endmodule



rewards "total_answered"
	choose_folder : 1;
	choose_bin : 1;
//	[easy_folder] true : 1;
//	[med_folder] true : 1;
//	[diff_folder] true : 1;
//	[med_bin] true : 1;
//	[diff_bin] true : 1;

endrewards

rewards "easy_folder_reward"
	[easy_folder] true : 1;
endrewards

rewards "med_folder_reward"
	[med_folder] true : 1;
endrewards

rewards "diff_folder_reward"
	[diff_folder] true : 1;
endrewards

rewards "med_bin_reward"
	[med_bin] true : 1;
endrewards

rewards "diff_bin_reward"
	[diff_bin] true : 1;
endrewards
