dtmc

// constants to establish the number of states
const int patient_state_max = 2;			// (number of state for the patient)
const int original_image_max = 15;		// ()
const int double_image_max = 5;		// ()
const int original_image_no_double_max = original_image_max-double_image_max;		// ()
const int image_max = original_image_max + double_image_max;		// ()
const int min_sep_orig_double = 2;	//
const int max_sep_orig_double = 3;	//

// boolean formulas
formula no_Original = original_no_double+original_double=original_image_max;
formula no_Original_No_Double = original_no_double=original_image_no_double_max;
formula no_Double = double_image=double_image_max;
formula nb_orig_nd = original_image_no_double_max-original_no_double;
formula nb_orig_d = double_image_max-original_double;


// formulas to determine the probability for each arc
formula p_original_no_double = (original_image_no_double_max - original_no_double)/(original_image_max - original_no_double - original_double);
formula p_original_double = (double_image_max - original_double)/(original_image_max - original_no_double - original_double);

//Markovian chain

module MeMo_game

	patient : [0..patient_state_max] init 0;		// state of the patient

	original_no_double : [0..original_image_no_double_max] init 0;	//
	original_double : [0..double_image_max] init 0;	//
	double_image : [0..double_image_max] init 0;	//
	is_original_double : bool init false;		//
	is_double : bool init false;		//

	separation : [0 .. max_sep_orig_double] init 0;	//
	serie : [0 .. max_sep_orig_double] init 0;	//

	//good_folder : bool init false;		//
	//bad_folder : bool init false;		//
	//good_bin : bool init false;		//
	//bad_bin : bool init false;		//
	quit_game : bool init false;		//


	[pressStart] patient=0 -> (patient'=1); // the patient presses the start button
//original_image_no_double_max-original_no_double<=double_image_max-original_double
//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
//separation = 0

	[acts1] patient=1 & quit_game=false & separation=0 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>=1 & nb_orig_nd=0)) -> p_original_no_double : (original_no_double'=original_no_double+1) & (is_original_double'=false) & (is_double'=false) + p_original_double : (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

	[acts2] patient=1 & quit_game=false & separation=0 & nb_orig_d=1 & nb_orig_nd=1 -> (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
//separation = 1

	[acts3] patient=1 & quit_game=false & separation=1 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>=1 & nb_orig_nd=0)) -> p_original_no_double : (original_no_double'=original_no_double+1)  & (separation'=separation+1) & (is_original_double'=false) & (is_double'=false) + p_original_double : (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

	[acts4] patient=1 & quit_game=false & separation=1 & nb_orig_d=1 & nb_orig_nd=1 -> (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
//separation = 2

	[acts5] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=1 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>1 & nb_orig_nd=0)) -> p_original_no_double/2 : (original_no_double'=original_no_double+1) & (separation'=separation+1) & (is_original_double'=false) & (is_double'=false) + p_original_double/2 : (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1) + 1/2 : (double_image'=double_image+1) & (serie'=serie-1) & (separation'=0) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts6] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=1 & nb_orig_d=1 & nb_orig_nd=1 -> 1/2 : (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1) + 1/2 : (double_image'=double_image+1) & (serie'=serie-1) & (separation'=0) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts7] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=1 & (nb_orig_d=1 & nb_orig_nd=0) -> (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

	[acts8] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=1 & (nb_orig_d=0 & nb_orig_nd=0) & no_Double=false -> (double_image'=double_image+1) & (serie'=serie-1) & (separation'=0) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts9] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=2 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>2 & nb_orig_nd=0)) -> p_original_no_double/2 : (original_no_double'=original_no_double+1) & (separation'=separation+1) & (is_original_double'=false) & (is_double'=false) + p_original_double/2 : (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1) + 1/2 : (double_image'=double_image+1) & (serie'=serie-1) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts10] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=2 & nb_orig_d=1 & nb_orig_nd=1  -> (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

	[acts11] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=2 & (nb_orig_d=2 & nb_orig_nd=0) -> (double_image'=double_image+1) & (serie'=serie-1) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts12] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=2 & (nb_orig_d=1 & nb_orig_nd=0) -> (original_double'=original_double+1) & (is_original_double'=true) & (is_double'=false) & (separation'=separation+1) & (serie'=serie+1); //while loop

	[acts13] patient=1 & quit_game=false & separation=min_sep_orig_double & serie=2 & (nb_orig_d=0 & nb_orig_nd=0) & no_Double=false -> (double_image'=double_image+1) & (serie'=serie-1) & (is_double'=true) & (is_original_double'=false); //while loop

//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
// separation = 3

	[acts14] patient=1 & quit_game=false & separation=max_sep_orig_double & serie=1 -> (double_image'=double_image+1) & (serie'=serie-1) & (separation'=0) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts15] patient=1 & quit_game=false & separation=max_sep_orig_double & serie=2 & is_original_double=false -> (double_image'=double_image+1) & (serie'=serie-1) & (is_double'=true); //while loop

	[acts16] patient=1 & quit_game=false & separation=max_sep_orig_double & serie=2 & is_original_double=true -> (double_image'=double_image+1) & (serie'=serie-1) & (separation'=separation-1) & (is_double'=true) & (is_original_double'=false); //while loop

	[acts17] patient=1 & quit_game=false & separation=max_sep_orig_double & serie=3 -> (double_image'=double_image+1) & (serie'=serie-1) & (is_double'=true) & (is_original_double'=false); //while loop


//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

	[endGame] patient=1 & no_Original & no_Double -> (patient'=2);

	//[leftGame] patient=1 & quit_game=true -> (patient'=patient+1) & (quit_game'=false); // the game ends

	[] patient=2 -> true; //ending the model, needed to avoid deadlocks during model-checking

endmodule

//probabilté pour joueur
const double weight_good_folder = 0.5;
const double weight_bad_bin = 0.25;
const double weight_good_bin = 0.25;
const double weight_bad_folder = 0.0005;

// formula for the sum of the previous weight
formula sum_weight_orig = weight_good_folder + weight_bad_bin;
formula sum_weight_doub = weight_good_bin + weight_bad_folder;

// formulas to determine the probability for each arc
formula proba_good_folder = weight_good_folder/sum_weight_orig;
formula proba_bad_bin = weight_bad_bin/sum_weight_orig;
formula proba_good_bin = weight_good_bin/sum_weight_doub;
formula proba_bad_folder = weight_bad_folder/sum_weight_doub;

module MeMo_patient

	patients : [0..patient_state_max] init 0;		// state of the patient
	good_folder : [0..original_image_max] init 0;
	bad_bin : [0..original_image_max] init 0;
	good_bin : [0..double_image_max] init 0;
	bad_folder : [0..double_image_max] init 0;
	//good_folder : bool init false;		//
	//bad_folder : bool init false;		//
	//good_bin : bool init false;		//
	//bad_bin : bool init false;		//
	//quit_game : bool init false;		//


	[pressStart] patients=0 -> (patients'=1); // the patient presses the start button
//original_image_no_double_max-original_no_double<=double_image_max-original_double
//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
//

	[original_pict] patient=1 -> proba_good_folder : (good_folder'=good_folder+1) + proba_bad_bin : (bad_bin'=bad_bin+1); //while loop

	[double_pict] patient=1 -> proba_good_bin : (good_bin'=good_bin+1) + proba_bad_folder : (bad_folder'=bad_folder+1); //while loop

//================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

	[endGame] patients=1 & no_Original & no_Double -> (patients'=2);

	//[leftGame] patient=1 & quit_game=true -> (patient'=patient+1) & (quit_game'=false); // the game ends

	[] patients=2 -> true; //ending the model, needed to avoid deadlocks during model-checking


endmodule

//Reward
