dtmc

// constants to establish the number of states
const int patient_state_max = 2;			// (number of state for the patient)
const int original_image_max = 15;
const int double_image_max = 5;
const int image_max = original_image_max + double_image_max;		// ()
const int original_image_no_double_max = original_image_max-double_image_max;		// ()
const int min_sep_orig_double = 2;	//
const int max_sep_orig_double = 3;	//


// boolean formulas
formula no_Original = original_no_double+original_double=original_image_max;
formula no_Original_No_Double = original_no_double=original_image_no_double_max;
formula no_Double = double_image=double_image_max;


formula nb_orig_nd = original_image_no_double_max-original_no_double;
formula nb_orig_d = double_image_max-original_double;
formula nb_orig = original_image_max-original_no_double-original_double;


//probabilté pour joueur
const double weight_good_folder = 8;
const double weight_bad_bin = 2;

const double weight_good_bin = 9;
const double weight_bad_folder = 1;

const double weight_quit = 0.0005;


// formula for the sum of the previous weight
formula sum_weight_orig = weight_good_folder + weight_bad_bin + weight_quit;

formula sum_weight_doub = weight_good_bin + weight_bad_folder + weight_quit;


// formulas to determine the probability for each arc
formula proba_good_folder = weight_good_folder/sum_weight_orig;
formula proba_bad_bin = weight_bad_bin/sum_weight_orig;
formula proba_quit_orig = weight_quit/sum_weight_orig;

formula proba_good_bin = weight_good_bin/sum_weight_doub;
formula proba_bad_folder = weight_bad_folder/sum_weight_doub;
formula proba_quit_doub = weight_quit/sum_weight_doub;


// formulas for the probabilities of the game
formula p_original_no_double = (original_image_no_double_max - original_no_double)/(original_image_max - original_no_double - original_double);
formula p_original_double = (double_image_max - original_double)/(original_image_max - original_no_double - original_double);


module Serious_game

	game_on_off : [0..1] init 0;
	
	is_original : bool init false;
	is_original_double : bool init false;
	will_original_double : bool init false;		//
	is_double : bool init false;		//
	original_no_double : [0..original_image_no_double_max] init 0;	//
	original_double : [0..double_image_max] init 0;	//
	double_image : [0..double_image_max] init 0;	//

	nb_original : [0..original_image_max] init 0;
	nb_doublon : [0..double_image_max] init 0;

	separation : [0 .. max_sep_orig_double] init 0;	//
	serie : [0 .. max_sep_orig_double] init 0;	//

	int_original : bool init false;
	int_doublon : bool init false;	

	[pressStart] game_on_off=0 -> (game_on_off'=1) & (int_original'=true);



	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & (separation=0 | separation=1) & nb_orig>=1 -> (int_original'=true) & (is_original'=false) & (is_double'=false);



	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=1 | serie=2) & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>1 & nb_orig_nd=0)) -> 1/2 : (int_original'=true) & (is_original'=false) & (is_double'=false) + 1/2 : (int_doublon'=true) & (is_original'=false) & (is_double'=false) ;

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=1) &  nb_orig_d=1 & nb_orig_nd=1 -> 1/2 : (int_original'=true) & (is_original'=false) & (is_double'=false) & (will_original_double'=true) + 1/2 : (int_doublon'=true) & (is_original'=false) & (is_double'=false) ;

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=1) & (nb_orig_d=1 & nb_orig_nd=0) -> (int_original'=true) & (is_original'=false) & (is_double'=false) & (will_original_double'=true);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=2) & ((nb_orig_d=1 & nb_orig_nd=1) | (nb_orig_d=1 & nb_orig_nd=0)) -> (int_original'=true) & (is_original'=false) & (is_double'=false) & (will_original_double'=true);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=2) & (((nb_orig_d=0 & nb_orig_nd=0) & no_Double=false) | (nb_orig_d=2 & nb_orig_nd=0)) -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=min_sep_orig_double & (serie=1) & (nb_orig_d=0 & nb_orig_nd=0) & no_Double=false -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);



	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=max_sep_orig_double & (serie=1) -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=max_sep_orig_double & (serie=2) & is_original_double=true -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & separation=max_sep_orig_double & ((serie=3)|((serie=2) & is_original_double=false)) -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);





	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & nb_doublon=double_image_max & nb_original<original_image_max -> (int_original'=true) & (is_original'=false) & (is_double'=false);

	[intermediate] game_on_off=1 & int_original=false & int_doublon=false & nb_doublon<double_image_max & nb_original=original_image_max -> (int_doublon'=true) & (is_original'=false) & (is_double'=false);



	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=0 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>=1 & nb_orig_nd=0)) -> p_original_double : (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (is_original_double'=true) + p_original_no_double : (is_original'=true) & (int_original'=false) & (original_no_double'=original_no_double+1) & (is_original_double'=false);

	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & (separation=0 | separation=1) & nb_orig_d=1 & nb_orig_nd=1 -> (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (is_original_double'=true);


	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=1 & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>=1 & nb_orig_nd=0)) -> p_original_double : (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (is_original_double'=true) + p_original_no_double : (is_original'=true) & (int_original'=false) & (original_no_double'=original_no_double+1) & (separation'=separation+1) & (is_original_double'=false);


	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=min_sep_orig_double & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>1 & nb_orig_d<3 & nb_orig_nd=0)) -> p_original_double : (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (is_original_double'=true) + p_original_no_double : (is_original'=true) & (int_original'=false) & (original_no_double'=original_no_double+1) & (separation'=separation+1) & (is_original_double'=false);

	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=min_sep_orig_double & ((nb_orig_d>1 & nb_orig_nd>=1) | (nb_orig_d=0 & nb_orig_nd>=1) | (nb_orig_d>=1 & nb_orig_nd>1) | (nb_orig_d>2 & nb_orig_nd=0)) -> p_original_double : (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (is_original_double'=true) + p_original_no_double : (is_original'=true) & (int_original'=false) & (original_no_double'=original_no_double+1) & (separation'=separation+1) & (is_original_double'=false);


	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=min_sep_orig_double & will_original_double=true -> (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (will_original_double'=false) & (is_original_double'=true);


	[original_pict] game_on_off=1 & int_original=true & int_doublon=false & separation=min_sep_orig_double & will_original_double=true -> (is_original'=true) & (int_original'=false) & (original_double'=original_double+1) & (separation'=separation+1) & (serie'=serie+1) & (will_original_double'=false) & (is_original_double'=true);





	[double_pict] game_on_off=1 & int_doublon=true & int_original=false & (serie=1) -> (is_double'=true) & (int_doublon'=false) & (double_image'=double_image+1) & (serie'=serie-1) & (separation'=0) & (is_original_double'=false);

	[double_pict] game_on_off=1 & int_doublon=true & int_original=false & (((serie=2) & separation=min_sep_orig_double) | (serie=3))  -> (is_double'=true) & (int_doublon'=false) & (double_image'=double_image+1) & (serie'=serie-1) & (is_original_double'=false);


	[double_pict] game_on_off=1 & int_doublon=true & int_original=false & (serie=2) & separation=max_sep_orig_double -> (is_double'=true) & (int_doublon'=false) & (double_image'=double_image+1) & (serie'=serie-1) & (separation'=separation-1) & (is_original_double'=false);


	[quitGame] game_on_off=1 -> (game_on_off'=0) & (is_original'=false) & (is_double'=false) & (int_original'=false) & (int_doublon'=false);

	[endGame] game_on_off=1 & no_Original & no_Double -> (game_on_off'=0) & (is_original'=false) & (is_double'=false) & (int_original'=false) & (int_doublon'=false);

	[patient_out] game_on_off=0 -> true;

endmodule


module MeMo_patient

	patient : [0..patient_state_max] init 0;		// state of the patient
	good_folder : [0..original_image_max] init 0;
	bad_bin : [0..original_image_max] init 0;
	good_bin : [0..double_image_max] init 0;
	bad_folder : [0..double_image_max] init 0;

	good_folder_bool : bool init false;
	bad_bin_bool : bool init false;
	good_bin_bool : bool init false;
	bad_folder_bool : bool init false;

	quit_game : bool init false;


	[pressStart] patient=0 -> (patient'=1); // the patient presses the start button

	[original_pict] patient=1 & good_folder+bad_bin+good_bin+bad_folder<image_max & quit_game=false -> proba_good_folder : (good_folder'=good_folder+1) & (good_folder_bool'=true) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false) + proba_bad_bin : (bad_bin'=bad_bin+1) & (good_folder_bool'=false) & (bad_bin_bool'=true) & (good_bin_bool'=false) & (bad_folder_bool'=false) + proba_quit_orig: (quit_game'=true) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);// + proba_quit_orig : (quit_game'=true); //while loop

	[double_pict] patient=1 & good_folder+bad_bin+good_bin+bad_folder<image_max & quit_game=false -> proba_good_bin : (good_bin'=good_bin+1) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=true) & (bad_folder_bool'=false) + proba_bad_folder : (bad_folder'=bad_folder+1) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=true) + proba_quit_doub: (quit_game'=true) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);// + proba_quit_doub : (quit_game'=true); //while loop

	[endGame] patient=1 & good_folder+bad_bin+good_bin+bad_folder=image_max & quit_game=false -> (patient'=2) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);

	[quitGame] patient=1 & quit_game=true -> (patient'=2) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);

	//[leftGame] patient=1 & quit_game=true -> (patient'=patient+1) & (quit_game'=false); // the game ends

	[patient_out] patient=2 -> true; //ending the model, needed to avoid deadlocks during model-checking


endmodule

//Reward


rewards "Good_folder_reward"
	good_folder_bool=true & is_original=true: 1;
endrewards

rewards "Bad_bin_reward"
	bad_bin_bool=true & is_original=true: 1;
endrewards

rewards "Good_bin_reward"
	good_bin_bool=true & is_double=true: 1;
endrewards

rewards "Bad_folder_reward"
	bad_folder_bool=true & is_double=true: 1;
endrewards



