dtmc

// constants to establish the number of states
const int patient_state_max = 2;			// (number of state for the patient)
const int original_image_max = 15;
const int double_image_max = 5;
const int image_max = 20;

//probabilté pour joueur
const double weight_good_folder = 8;
const double weight_bad_bin = 2;

const double weight_good_bin = 9;
const double weight_bad_folder = 1;

const double weight_quit = 0.0005;

// formula for the sum of the previous weight
formula sum_weight_orig = weight_good_folder + weight_bad_bin + weight_quit;

formula sum_weight_doub = weight_good_bin + weight_bad_folder + weight_quit;

// formulas to determine the probability for each arc
formula proba_good_folder = weight_good_folder/sum_weight_orig;
formula proba_bad_bin = weight_bad_bin/sum_weight_orig;
formula proba_quit_orig = weight_quit/sum_weight_orig;

formula proba_good_bin = weight_good_bin/sum_weight_doub;
formula proba_bad_folder = weight_bad_folder/sum_weight_doub;
formula proba_quit_doub = weight_quit/sum_weight_doub;


module Hasard_game

	state : [0..2] init 0;
	original : bool init false;
	nb_original : [0..original_image_max] init 0;
	doublon : bool init false;
	nb_doublon : [0..double_image_max] init 0;
	int_original : bool init false;
	int_doublon : bool init false;	

	[pressStart] state=0 -> 0.5 : (state'=1) & (int_original'=true) + 0.5 : (state'=1) & (int_doublon'=true);

	[intermediate] state=1 & int_original=false & int_doublon=false & nb_doublon<double_image_max & nb_original<original_image_max -> 0.5 : (int_original'=true) & (original'=false) & (doublon'=false) + 0.5 : (int_doublon'=true) & (original'=false) & (doublon'=false);

	[intermediate] state=1 & int_original=false & int_doublon=false & nb_doublon=double_image_max & nb_original<original_image_max -> (int_original'=true) & (original'=false) & (doublon'=false);

	[intermediate] state=1 & int_original=false & int_doublon=false & nb_doublon<double_image_max & nb_original=original_image_max -> (int_doublon'=true) & (original'=false) & (doublon'=false);

	[original_pict] state=1 & int_original=true & int_doublon=false -> (original'=true) & (int_original'=false) & (nb_original'=nb_original+1);

	[double_pict] state=1 & int_doublon=true & int_original=false -> (doublon'=true) & (int_doublon'=false) & (nb_doublon'=nb_doublon+1);

	[quitGame] state=1 -> (state'=2) & (original'=false) & (doublon'=false) & (int_original'=false) & (int_doublon'=false);

	[endGame] state=1 & nb_original+nb_doublon=image_max-> (state'=2) & (original'=false) & (doublon'=false) & (int_original'=false) & (int_doublon'=false);

	[patient_out] state=2 -> true;

endmodule


module MeMo_patient

	patient : [0..patient_state_max] init 0;		// state of the patient
	good_folder : [0..original_image_max] init 0;
	bad_bin : [0..original_image_max] init 0;
	good_bin : [0..double_image_max] init 0;
	bad_folder : [0..double_image_max] init 0;

	good_folder_bool : bool init false;
	bad_bin_bool : bool init false;
	good_bin_bool : bool init false;
	bad_folder_bool : bool init false;

	quit_game : bool init false;


	[pressStart] patient=0 -> (patient'=1); // the patient presses the start button

	[original_pict] patient=1 & good_folder+bad_bin+good_bin+bad_folder<image_max & quit_game=false -> proba_good_folder : (good_folder'=good_folder+1) & (good_folder_bool'=true) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false) + proba_bad_bin : (bad_bin'=bad_bin+1) & (good_folder_bool'=false) & (bad_bin_bool'=true) & (good_bin_bool'=false) & (bad_folder_bool'=false) + proba_quit_orig: (quit_game'=true) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);// + proba_quit_orig : (quit_game'=true); //while loop

	[double_pict] patient=1 & good_folder+bad_bin+good_bin+bad_folder<image_max & quit_game=false -> proba_good_bin : (good_bin'=good_bin+1) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=true) & (bad_folder_bool'=false) + proba_bad_folder : (bad_folder'=bad_folder+1) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=true) + proba_quit_doub: (quit_game'=true) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);// + proba_quit_doub : (quit_game'=true); //while loop

	[endGame] patient=1 & good_folder+bad_bin+good_bin+bad_folder=image_max & quit_game=false -> (patient'=2) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);

	[quitGame] patient=1 & quit_game=true -> (patient'=2) & (good_folder_bool'=false) & (bad_bin_bool'=false) & (good_bin_bool'=false) & (bad_folder_bool'=false);

	//[leftGame] patient=1 & quit_game=true -> (patient'=patient+1) & (quit_game'=false); // the game ends

	[patient_out] patient=2 -> true; //ending the model, needed to avoid deadlocks during model-checking


endmodule

//Reward


rewards "Good_folder_reward"
	good_folder_bool=true & original=true: 1;
endrewards

rewards "Bad_bin_reward"
	bad_bin_bool=true & original=true: 1;
endrewards

rewards "Good_bin_reward"
	good_bin_bool=true & doublon=true: 1;
endrewards

rewards "Bad_folder_reward"
	bad_folder_bool=true & doublon=true: 1;
endrewards



