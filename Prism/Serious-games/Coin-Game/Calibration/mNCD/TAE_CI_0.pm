dtmc

//const int num_targ_training = 2; => neglected to simplify computations
const int num_targ_max = 10;// + num_targ_training;
//const int num_deco_training = 1; => neglected to simplify computations
const int num_deco_max = 5;// + num_deco_training;
const int num_sign_max = num_targ_max + num_deco_max;
//const int num_train_max = num_targ_training + num_deco_training; => neglected to simplify computations


//const int click_time_max = 5; // The patient got 1500ms to click on the picture
const int anticipation = 1; // If the patient clicks during the first 150ms, we consider it was anticipation
//const int overlap_max = click_time_max-time_betw_sign_min; //maximum duration of an overlap between clickable time and display of new picture

formula num_sign = num_targ + num_deco;

//formula proba_next_train_targ = (num_targ_training - num_targ) / (num_targ_training + num_deco_training - num_sign);
//formula proba_next_train_deco = (num_deco_training - num_deco) / (num_targ_training + num_deco_training - num_sign);

formula proba_next_targ = (num_targ_max - num_targ) / (num_sign_max - num_sign);
formula proba_next_deco = (num_deco_max - num_deco) / (num_sign_max - num_sign);



//As speed of signal was not saved in the clinical experiment, this variable will be put aside 
module game

	game_on : bool init false; // is the game on or off

//	fast_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete
//	medi_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete
//	slow_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete

	next_targ : bool init false;
	next_deco : bool init false;
	next_end : bool init false; // To determine if in next step, it is end of the game

	//num_sign : [0..num_sign_max] init 0; // How many signals did appear
	num_targ : [0..num_targ_max] init 0; // How many signals did appear
	num_deco : [0..num_deco_max] init 0; // How many signals did appear


	//Start game
	[pressStart] game_on=false & next_end=false & num_sign=0 -> proba_next_targ : (game_on'=true) & (next_targ'=true) + proba_next_deco : (game_on'=true) & (next_deco'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstTarg] game_on=true & num_sign=0 & next_targ=true -> (num_targ'=num_targ+1) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstDeco] game_on=true & num_sign=0 & next_deco=true -> (num_deco'=num_deco+1) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition a non training signal
	[transiting] game_on=true & num_sign>0 & num_sign<num_sign_max & next_deco=false & next_targ=false & next_end=false -> proba_next_targ : (next_targ'=true) + proba_next_deco : (next_deco'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[Targ] game_on=true & num_sign>=0 & num_sign<num_sign_max & next_targ=true -> (num_targ'=num_targ+1) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[Deco] game_on=true & num_sign>=0 & num_sign<num_sign_max & next_deco=true -> (num_deco'=num_deco+1) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[End] game_on=true & num_sign=num_sign_max & next_end=false-> (next_end'=true);

/////////////////////////////////////////////////////////////////////
	//End of the game
	[endGame] game_on=true & next_end=true -> (game_on'=false);

	//End loop
	[endLoop] game_on=false & next_end=true -> true;

endmodule


/////////////////////////////////////////////////////////////////////

//probability for the patient to click exactly once for the previous target
formula proba_anticipate = 0.0005;//(0.05*prev_time_betw_sign/time_betw_sign_max) + (0.10*total_time/total_time_max) + (0.05*num_sign/num_sign_max);
//probability for the patient to click exactly once for the previous target
formula proba_train_anticipate = 0.15 - (0.07*num_sign/3) + (0.05);///num_train_max) + (0.05*prev_time_betw_sign/time_betw_sign_max);


//////////////////////////////////////

//probability for the patient to not click at all for the previous deco
formula proba_first_not_click = 0.87;

//probability for the patient to click exactly once for the previous deco
formula proba_first_click_slow = 0.10;

//probability for the patient to double click for the previous deco
formula proba_double_click_first = 0.02;

//probability for the patient to click several time for the previous deco
formula proba_several_click_first = 0.01;



//////////////////////////////////////

//probability for the patient to click exactly once for the previous training target
formula proba_train_good_click_fast = (0.19 + (0.09*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click exactly once for the previous training target
formula proba_train_good_click_slow = (0.31 + (0.14*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to not click at all for the previous training target
formula proba_train_bad_not_click = (0.10 - (0.05*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to double click for the previous training target
formula proba_train_double_click_targ = (0.20 - (0.11*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click several time for the previous training target
formula proba_train_several_click_targ = (0.20 - (0.07*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//////////////////////////////////////

//probability for the patient to click exactly once for the previous training deco
formula proba_train_bad_click_fast = (0.45 - (0.05*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click exactly once for the previous training deco
formula proba_train_bad_click_slow = (0.24 - (0.03*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to not click at all for the previous training deco
formula proba_train_good_not_click = (0.10 + (0.10*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to double click for the previous training deco
formula proba_train_double_click_deco = (0.15 - (0.01*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click several time for the previous training deco
formula proba_train_several_click_deco = (0.06 - (0.01*num_sign))*(1-proba_train_anticipate);///num_train_max))*(1-proba_train_anticipate);





//////////////////////////////////////

const mu =34;
formula sigma = 9;

//formula gaussian_probability = (1/(sigma*pow(2*3.141,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));
formula gaussian_probability_targ = (1/(sigma*pow(2*0.2,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));

//probability for the patient to click exactly once for the previous target
formula proba_good_click_fast = 0.40/(1+proba_anticipate);//(0.25 + (gaussian_probability_targ)- (0.08 *((num_sign+total_time)/50)))*(1-proba_anticipate);

//probability for the patient to click exactly once for the previous target
formula proba_good_click_slow = 0.30/(1+proba_anticipate);//((0.5 - gaussian_probability_targ)- (0.05 *((num_sign+total_time)/50)))*(1-proba_anticipate);//((1/3*gaussian_probability))*(1-proba_anticipate);

//probability for the patient to not click at all for the previous target
formula proba_bad_not_click = 0.15/(1+proba_anticipate);//1/3 * (1-proba_good_click_fast-proba_good_click_slow-proba_several_click_targ-proba_anticipate);//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);

//probability for the patient to double click for the previous target
formula proba_double_click_targ = 0.10/(1+proba_anticipate);//2/3 * (1-proba_good_click_fast-proba_good_click_slow-proba_several_click_targ-proba_anticipate);//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);

//probability for the patient to click several time for the previous target
formula proba_several_click_targ = 0.05/(1+proba_anticipate);//(0.175-(0.0075*(150/(num_sign+total_time))));//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);


//////////////////////////////////////

const mu_2 =24;
formula sigma_2 = 6;

formula gaussian_probability = (1/(sigma*pow(2*3.141,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));
formula gaussian_probability_decoy = (1/(sigma_2*pow(2*0.2,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu_2)/sigma_2,2));

//probability for the patient to click exactly once for the previous deco
formula proba_bad_click_fast = (0.6 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to click exactly once for the previous deco
formula proba_bad_click_slow = (0.3 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to not click at all for the previous deco
formula proba_good_not_click = (0.10 + (gaussian_probability_decoy))*(1-proba_anticipate);

//probability for the patient to double click for the previous deco
formula proba_double_click_deco = (0.03 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to click several time for the previous deco
formula proba_several_click_deco = (0.07 * (1-proba_anticipate - proba_good_not_click));



module patient
	will_play : bool init true;
	front_screen : bool init false;


	anticipate : bool init false;
	click_fast : bool init false;
	click_slow : bool init false;
	not_click : bool init false;
	double_click : bool init false;
	several_click : bool init false;


	curr_targ : bool init false;
	curr_deco : bool init false;
	prev_targ : bool init false;
	prev_deco : bool init false;
	prev_none : bool init false;


	//Start game
	[pressStart] will_play=true & front_screen=false -> (front_screen'=true);

	[transiting] true -> (prev_targ'=curr_targ) & (prev_deco'=curr_deco) & (prev_none'=false);

	[firstTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (several_click'=true);

	[firstDeco] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_deco'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_deco'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_deco'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_deco'=true) & (several_click'=true);

	[Targ] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[Targ] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[Deco] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[Deco] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[End] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[End] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[endGame] will_play=true & front_screen=true -> (will_play'=false) & (front_screen'=false) & (prev_targ'=false) & (prev_deco'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false);

	[endLoop] will_play=false & front_screen=false -> true;

endmodule


const int time_betw_sign_max = 3;
const int total_time_max = (num_sign_max+1)*time_betw_sign_max;


module observer

	total_time : [0..total_time_max] init 0;

//	time_betw_sign : [0..time_betw_sign_max] init 0;

//	prev_time_betw_sign : [0..time_betw_sign_max] init 0;

	num_action : [0..num_sign_max+1] init 0;

	num_act_targ : [0..num_targ_max] init 0;

	num_act_deco : [0..num_deco_max] init 0;

	memo_targ : [0..1] init 0;

	memo_deco : [0..1] init 0;

	transiting : bool init false;

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
//	[firstTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);
	[firstTarg] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);

/////////////////////////////////////////////////////////////////////

//	[transiting] true -> (prev_time_betw_sign'=time_betw_sign) & (transiting'=true) & (num_act_targ'=num_act_targ+memo_targ) & (num_act_deco'=num_act_deco+memo_deco);
	[transiting] true -> (transiting'=true) & (num_act_targ'=num_act_targ+memo_targ) & (num_act_deco'=num_act_deco+memo_deco);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
//	[firstDeco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);
	[firstDeco] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
//	[Targ] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);
	[Targ] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
//	[Deco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);
	[Deco] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
//	[End] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1);
	[End] true -> (transiting'=false) & (num_action'=num_action+1);

/////////////////////////////////////////////////////////////////////

	//End loop
	[endLoop] true -> true;

endmodule


rewards "good_on_target"
	prev_targ & (click_fast|click_slow) & transiting=false : 1;
endrewards

rewards "good_on_decoy"
	prev_deco & (not_click) & transiting=false : 1;
endrewards

rewards "good_on_signal"
	prev_deco & (not_click) & transiting=false : 1;
	prev_targ & (click_fast|click_slow) & transiting=false : 1;
endrewards

rewards "bad_on_target"
	prev_targ & !click_fast & !click_slow & transiting=false : 1;
endrewards

rewards "bad_on_decoy"
	prev_deco & !not_click & transiting=false : 1;
endrewards

rewards "bad_on_signal"
	prev_deco & !not_click & transiting=false : 1;
	prev_targ & !click_fast & !click_slow & transiting=false : 1;
endrewards



// Probabilité que patient clique une fois sur chaque cible

// Probabilité détats futurs où patient clique plusieurs fois sur decoract

// Probabilité que patient clique plusieurs fois sur chaque decoract

// Probabilité que patient clique plusieurs fois sur chaque signal

// Reward combien de fois il fait juste pour cible

// Reward combien de fois il fait juste pour decoracteur

// Reward combien de fois il fait juste pour signal
