dtmc

//const int num_targ_training = 2; => neglected to simplify computations
const int num_targ_max = 30;// + num_targ_training;
//const int num_deco_training = 1; => neglected to simplify computations
const int num_deco_max = 10;// + num_deco_training;
const int num_sign_max = num_targ_max + num_deco_max;
//const int num_train_max = num_targ_training + num_deco_training; => neglected to simplify computations


//const int click_time_max = 5; // The patient got 1500ms to click on the picture
const int anticipation = 1; // If the patient clicks during the first 150ms, we consider it was anticipation
//const int overlap_max = click_time_max-time_betw_sign_min; //maximum duration of an overlap between clickable time and display of new picture

formula num_sign = num_targ + num_deco;

//formula proba_next_train_targ = (num_targ_training - num_targ) / (num_targ_training + num_deco_training - num_sign);
//formula proba_next_train_deco = (num_deco_training - num_deco) / (num_targ_training + num_deco_training - num_sign);

formula proba_next_targ = (num_targ_max - num_targ) / (num_sign_max - num_sign);
formula proba_next_deco = (num_deco_max - num_deco) / (num_sign_max - num_sign);



//As speed of signal was not saved in the clinical experiment, this variable will be put aside
module game

	game_on : bool init false; // is the game on or off

//	fast_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete
//	medi_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete
//	slow_apparition : bool init false; //This boolean determines the time between the previous picture and the current one => obsolete

	next_targ : bool init false;
	next_deco : bool init false;
	next_end : bool init false; // To determine if in next step, it is end of the game

	//num_sign : [0..num_sign_max] init 0; // How many signals did appear
	num_targ : [0..num_targ_max] init 0; // How many signals did appear
	num_deco : [0..num_deco_max] init 0; // How many signals did appear


	//Start game
	[pressStart] game_on=false & next_end=false & num_sign=0 -> (game_on'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstTarg] game_on=true & num_sign=0 & next_targ=true -> (num_targ'=num_targ+1) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstDeco] game_on=true & num_sign=0 & next_deco=true -> (num_deco'=num_deco+1) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition a non training signal
	[transiting] game_on=true & num_sign>=0 & num_sign<num_sign_max & next_deco=false & next_targ=false & next_end=false -> proba_next_targ : (next_targ'=true) + proba_next_deco : (next_deco'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[Targ] game_on=true & num_sign>=0 & num_sign<num_sign_max & next_targ=true -> (num_targ'=num_targ+1) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[Deco] game_on=true & num_sign>=0 & num_sign<num_sign_max & next_deco=true -> (num_deco'=num_deco+1) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[End] game_on=true & num_sign=num_sign_max & next_end=false-> (next_end'=true);

/////////////////////////////////////////////////////////////////////
	//End of the game
	[endGame] game_on=true & next_end=true -> (game_on'=false);

	//End loop
	[endLoop] game_on=false & next_end=true -> true;

endmodule


/////////////////////////////////////////////////////////////////////

formula proba_sfast = 01/1000;
formula proba_fast = 37/100;
formula proba_medium = 37/1000;
formula proba_slow = 333/1000;
formula proba_sslow = 259/1000;

formula proba_antici = 0185/100000;
formula proba_weight_click_targ = (9785/10000);
formula proba_click_targ = proba_weight_click_targ*(1-proba_antici);
formula proba_no_click_targ = (1-proba_weight_click_targ)*(1-proba_antici);
formula proba_weight_click_dec = (199/1000);
formula proba_click_dec = proba_weight_click_dec*(1-proba_antici);
formula proba_no_click_dec = (1-proba_weight_click_dec)*(1-proba_antici);

formula proba_click_targ_sfast = proba_sfast*proba_click_targ;
formula proba_click_targ_fast = proba_fast*proba_click_targ;
formula proba_click_targ_medium = proba_medium*proba_click_targ;
formula proba_click_targ_slow = proba_slow*proba_click_targ;
formula proba_click_targ_sslow = proba_sslow*proba_click_targ;

/////////////////////////////////////////////////////////////////////

formula proba_click_deco_sfast = proba_sfast*proba_click_dec;
formula proba_click_deco_fast = proba_fast*proba_click_dec;
formula proba_click_deco_medium = proba_medium*proba_click_dec;
formula proba_click_deco_slow = proba_slow*proba_click_dec;
formula proba_click_deco_sslow = proba_sslow*proba_click_dec;

/////////////////////////////////////////////////////////////////////



module patient_test
	will_play : bool init true;
	front_screen : bool init false;


	anticipate : bool init false;
	click_sfast : bool init false;
	click_fast : bool init false;
	click_medi :bool init false;
	click_slow : bool init false;
	click_sslow : bool init false;
	not_click : bool init false;




	curr_targ : bool init false;
	curr_deco : bool init false;
	prev_targ : bool init false;
	prev_deco : bool init false;
	prev_none : bool init false;
	count_targ : [0..4] init 0;
	count_deco : [0..1] init 0;
	//faire des lignes de code simple puis avec compteur.

	//Start game
	[pressStart] will_play=true & front_screen=false -> (front_screen'=true);

	[transiting] true -> (prev_targ'=curr_targ) & (prev_deco'=curr_deco) & (prev_none'=false);


	[firstTarg] will_play=true & front_screen=true -> (prev_none'=true) & (curr_targ'=true) & (not_click'=true);

	[firstDeco] will_play=true & front_screen=true -> (prev_none'=true) & (curr_deco'=true) & (not_click'=true);

	[Targ] will_play=true & front_screen=true & prev_targ=true -> proba_antici : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sfast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_medium : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sslow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);

	[Targ] will_play=true & front_screen=true & prev_deco=true -> proba_antici : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sfast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_medium : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sslow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_dec : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);

//	[Targ] will_play=true & front_screen=true & prev_deco=true -> proba_antici : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true);


	[Deco] will_play=true & front_screen=true & prev_targ=true -> proba_antici : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sfast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_medium : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sslow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);

	[Deco] will_play=true & front_screen=true & prev_deco=true -> proba_antici : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sfast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_medium : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sslow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_dec : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);

//	[Deco] will_play=true & front_screen=true & prev_deco=true -> proba_antici : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) + proba_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) + proba_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) + proba_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true);


	[End] will_play=true & front_screen=true & prev_targ=true -> proba_antici : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sfast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_medium : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_targ_sslow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);

	[End] will_play=true & front_screen=true & prev_deco=true -> proba_antici : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sfast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=true) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=true) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_medium : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=true) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=true) & (click_sslow'=false) & (not_click'=false) + proba_click_deco_sslow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=true) & (not_click'=false) + proba_no_click_dec : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=true);


	[endGame] will_play=true & front_screen=true -> (will_play'=false) & (front_screen'=false) & (prev_targ'=false) & (prev_deco'=false) & (anticipate'=false) & (click_sfast'=false) & (click_fast'=false) & (click_medi'=false) & (click_slow'=false) & (click_sslow'=false) & (not_click'=false);

	[endLoop] will_play=false & front_screen=false -> true;

endmodule


const int time_betw_sign_max = 3;
const int total_time_max = (num_sign_max+1)*time_betw_sign_max;


module observer

	total_time : [0..total_time_max] init 0;

//	time_betw_sign : [0..time_betw_sign_max] init 0;

//	prev_time_betw_sign : [0..time_betw_sign_max] init 0;

	num_action : [0..num_sign_max+1] init 0;

	num_act_targ : [0..num_targ_max] init 0;

	num_act_deco : [0..num_deco_max] init 0;

	memo_targ : [0..1] init 0;

	memo_deco : [0..1] init 0;

	transiting : bool init false;

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
//	[firstTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);
	[firstTarg] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);

/////////////////////////////////////////////////////////////////////

//	[transiting] true -> (prev_time_betw_sign'=time_betw_sign) & (transiting'=true) & (num_act_targ'=num_act_targ+memo_targ) & (num_act_deco'=num_act_deco+memo_deco);
	[transiting] true -> (transiting'=true) & (num_act_targ'=num_act_targ+memo_targ) & (num_act_deco'=num_act_deco+memo_deco);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
//	[firstDeco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);
	[firstDeco] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[Targ] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[Deco] true -> (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[End] true -> (transiting'=false) & (num_action'=num_action+1);

/////////////////////////////////////////////////////////////////////

	//End loop
	[endLoop] true -> true;

endmodule


rewards "good_on_target"
	prev_targ & (click_sfast|click_fast|click_medi|click_slow|click_sslow) & transiting=false : 1;
endrewards

rewards "good_on_decoy"
	prev_deco & (not_click) & transiting=false : 1;
endrewards

rewards "good_on_signal"
	prev_deco & (not_click) & transiting=false : 1;
	prev_targ & (click_sfast|click_fast|click_medi|click_slow|click_sslow) & transiting=false : 1;
endrewards

rewards "bad_on_target"
	prev_targ & (not_click|anticipate) & transiting=false : 1;
endrewards

rewards "bad_on_decoy"
	prev_deco & !not_click & transiting=false : 1;
endrewards

rewards "bad_on_signal"
	prev_deco & !not_click & transiting=false : 1;
	prev_targ & (not_click|anticipate) & transiting=false : 1;
endrewards

rewards "anticipate_decoy"
	prev_deco & anticipate & transiting=false : 1;
endrewards

rewards "anticipate_target"
	prev_targ & anticipate & transiting=false : 1;
endrewards

rewards "anticipate_all"
	prev_deco & anticipate & transiting=false : 1;
	prev_targ & anticipate & transiting=false : 1;
endrewards

rewards "sfast_on_target"
	prev_targ & (click_sfast) & transiting=false : 1;
endrewards

rewards "fast_on_target"
	prev_targ & (click_fast) & transiting=false : 1;
endrewards

rewards "medi_on_target"
	prev_targ & (click_medi) & transiting=false : 1;
endrewards

rewards "slow_on_target"
	prev_targ & (click_slow) & transiting=false : 1;
endrewards

rewards "sslow_on_target"
	prev_targ & (click_sslow) & transiting=false : 1;
endrewards


// Probabilité que patient clique une fois sur chaque cible

// Probabilité détats futurs où patient clique plusieurs fois sur decoract

// Probabilité que patient clique plusieurs fois sur chaque decoract

// Probabilité que patient clique plusieurs fois sur chaque signal

// Reward combien de fois il fait juste pour cible

// Reward combien de fois il fait juste pour decoracteur

// Reward combien de fois il fait juste pour signal
