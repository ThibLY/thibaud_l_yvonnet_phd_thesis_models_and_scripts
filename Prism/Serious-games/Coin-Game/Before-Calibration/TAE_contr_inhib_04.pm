dtmc

const int num_targ_training = 3;
const int num_targ_max = 17 + num_targ_training;
const int num_deco_training = 1;
const int num_deco_max = 5 + num_deco_training;
const int num_sign_max = num_targ_max + num_deco_max;
const int num_train_max = num_targ_training + num_deco_training;
//const int disp_time = 2; // Represents the 300ms of display time for each signals
//const int apparition_time = 13; // Represents the 2000ms separating each signal
//const int app_time_var = 10; // Represents the 1500ms variation for apparition of each signal (2000 +- 1500)
//const int time_betw_sign_max = disp_time+apparition_time+app_time_var; //the maximum time we can have between two pictures
//const int time_betw_sign_min = disp_time+apparition_time-app_time_var; //the maximum time we can have between two pictures


//const int click_time_max = 5; // The patient got 1500ms to click on the picture
const int anticipation = 1; // If the patient clicks during the first 150ms, we consider it was anticipation
//const int overlap_max = click_time_max-time_betw_sign_min; //maximum duration of an overlap between clickable time and display of new picture

formula num_sign = num_targ + num_deco;

formula proba_next_train_targ = (num_targ_training - num_targ) / (num_targ_training + num_deco_training - num_sign);
formula proba_next_train_deco = (num_deco_training - num_deco) / (num_targ_training + num_deco_training - num_sign);

formula proba_next_targ = (num_targ_max - num_targ) / (num_sign_max - num_sign);
formula proba_next_deco = (num_deco_max - num_deco) / (num_sign_max - num_sign);



module game

	game_on : bool init false; // is the game on or off

	fast_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	medi_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	slow_apparition : bool init false; //This boolean determines the time between the previous picture and the current one

	next_targ : bool init false;
	next_deco : bool init false;
	next_end : bool init false; // To determine if in next step, it is end of the game

	//num_sign : [0..num_sign_max] init 0; // How many signals did appear
	num_targ : [0..num_targ_max] init 0; // How many signals did appear
	num_deco : [0..num_deco_max] init 0; // How many signals did appear


	//Start game
	[pressStart] game_on=false & next_end=false & num_sign=0 -> 1/3*proba_next_targ : (game_on'=true) & (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_deco : (game_on'=true) & (fast_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (game_on'=true) & (medi_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (game_on'=true) & (slow_apparition'=true) & (next_deco'=true);


/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastTarg] game_on=true & num_sign=0 & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediTarg] game_on=true & num_sign=0 & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowTarg] game_on=true & num_sign=0 & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastDeco] game_on=true & num_sign=0 & fast_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (fast_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediDeco] game_on=true & num_sign=0 & medi_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (medi_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowDeco] game_on=true & num_sign=0 & slow_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (slow_apparition'=false) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition of a training signal
	[transiting] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3*proba_next_train_targ : (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_train_targ : (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_train_targ : (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_train_deco : (fast_apparition'=true) & (next_deco'=true) + 1/3*proba_next_train_deco : (medi_apparition'=true) & (next_deco'=true) + 1/3*proba_next_train_deco : (slow_apparition'=true) & (next_deco'=true);

//////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[trainFastTarg] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[trainMediTarg] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[trainSlowTarg] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[trainFastDeco] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & fast_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (fast_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is too soon for the patient to click
	[trainMediDeco] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & medi_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (medi_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[trainSlowDeco] game_on=true & num_sign>=1 & num_sign<(num_targ_training + num_deco_training) & slow_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (slow_apparition'=false) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition a non training signal
	[transiting] game_on=true & num_sign=(num_targ_training + num_deco_training) & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3*proba_next_targ : (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_deco : (fast_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (medi_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (slow_apparition'=true) & (next_deco'=true);

	[transiting] game_on=true & num_sign>(num_targ_training + num_deco_training) & num_sign<num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3*proba_next_targ : (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_deco : (fast_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (medi_apparition'=true) & (next_deco'=true) + 1/3*proba_next_deco : (slow_apparition'=true) & (next_deco'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastTarg] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediTarg] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowTarg] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastDeco] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & fast_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (fast_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediDeco] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & medi_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (medi_apparition'=false) & (next_deco'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowDeco] game_on=true & num_sign>=(num_targ_training + num_deco_training) & num_sign<num_sign_max & slow_apparition=true & next_deco=true -> (num_deco'=num_deco+1) & (slow_apparition'=false) & (next_deco'=false);

/////////////////////////////////////////////////////////////////////
	//Transition to end
	[transiting] game_on=true & num_sign=num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3 : (fast_apparition'=true) + 1/3 : (medi_apparition'=true) + 1/3 : (slow_apparition'=true);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastEnd] game_on=true & num_sign=num_sign_max & fast_apparition=true -> (next_end'=true) & (fast_apparition'=false);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediEnd] game_on=true & num_sign=num_sign_max & medi_apparition=true -> (next_end'=true) & (medi_apparition'=false);

	//Prepare for end of game
	[slowEnd] game_on=true & num_sign=num_sign_max & slow_apparition=true -> (next_end'=true) & (slow_apparition'=false);

/////////////////////////////////////////////////////////////////////
	//End of the game
	[endGame] game_on=true & next_end=true -> (game_on'=false);

	//End loop
	[endLoop] game_on=false & next_end=true -> true;

endmodule


/////////////////////////////////////////////////////////////////////

//probability for the patient to click exactly once for the previous target
formula proba_anticipate = 0.05 + (0.05*prev_time_betw_sign/time_betw_sign_max) + (0.10*total_time/total_time_max) + (0.05*num_sign/num_sign_max);
//probability for the patient to click exactly once for the previous target
formula proba_train_anticipate = 0.15 - (0.07*num_sign/num_train_max) + (0.05*prev_time_betw_sign/time_betw_sign_max);


//////////////////////////////////////

//probability for the patient to not click at all for the previous deco
formula proba_first_not_click = 0.87;

//probability for the patient to click exactly once for the previous deco
formula proba_first_click_slow = 0.10;

//probability for the patient to double click for the previous deco
formula proba_double_click_first = 0.02;

//probability for the patient to click several time for the previous deco
formula proba_several_click_first = 0.01;



//////////////////////////////////////

//probability for the patient to click exactly once for the previous training target
formula proba_train_good_click_fast = (0.19 + (0.09*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click exactly once for the previous training target
formula proba_train_good_click_slow = (0.31 + (0.14*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to not click at all for the previous training target
formula proba_train_bad_not_click = (0.10 - (0.05*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to double click for the previous training target
formula proba_train_double_click_targ = (0.20 - (0.11*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click several time for the previous training target
formula proba_train_several_click_targ = (0.20 - (0.07*num_sign/num_train_max))*(1-proba_train_anticipate);

//////////////////////////////////////

//probability for the patient to click exactly once for the previous training deco
formula proba_train_bad_click_fast = (0.45 - (0.05*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click exactly once for the previous training deco
formula proba_train_bad_click_slow = (0.24 - (0.03*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to not click at all for the previous training deco
formula proba_train_good_not_click = (0.10 + (0.10*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to double click for the previous training deco
formula proba_train_double_click_deco = (0.15 - (0.01*num_sign/num_train_max))*(1-proba_train_anticipate);

//probability for the patient to click several time for the previous training deco
formula proba_train_several_click_deco = (0.06 - (0.01*num_sign/num_train_max))*(1-proba_train_anticipate);





//////////////////////////////////////

const mu =34;
formula sigma = 9;

//formula gaussian_probability = (1/(sigma*pow(2*3.141,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));
formula gaussian_probability_targ = (1/(sigma*pow(2*0.2,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));

//probability for the patient to click exactly once for the previous target
formula proba_good_click_fast = (0.25 + (gaussian_probability_targ)- (0.08 *((num_sign+total_time)/50)))*(1-proba_anticipate);

//probability for the patient to click exactly once for the previous target
formula proba_good_click_slow = ((0.5 - gaussian_probability_targ)- (0.05 *((num_sign+total_time)/50)))*(1-proba_anticipate);//((1/3*gaussian_probability))*(1-proba_anticipate);

//probability for the patient to not click at all for the previous target
formula proba_bad_not_click = 1/3 * (1-proba_good_click_fast-proba_good_click_slow-proba_several_click_targ-proba_anticipate);//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);

//probability for the patient to double click for the previous target
formula proba_double_click_targ = 2/3 * (1-proba_good_click_fast-proba_good_click_slow-proba_several_click_targ-proba_anticipate);//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);

//probability for the patient to click several time for the previous target
formula proba_several_click_targ = (0.175-(0.0075*(150/(num_sign+total_time))));//(1/3*(1 - proba_good_click_fast - proba_good_click_slow))*(1-proba_anticipate);

//

//////////////////////////////////////

const mu_2 =24;
formula sigma_2 = 6;

//formula gaussian_probability = (1/(sigma*pow(2*3.141,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu)/sigma,2));
formula gaussian_probability_decoy = (1/(sigma_2*pow(2*0.2,1/2)))*pow(1.359,-pow(((num_sign+total_time)-mu_2)/sigma_2,2));

//probability for the patient to click exactly once for the previous deco
formula proba_bad_click_fast = (0.6 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to click exactly once for the previous deco
formula proba_bad_click_slow = (0.3 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to not click at all for the previous deco
formula proba_good_not_click = (0.10 + (gaussian_probability_decoy))*(1-proba_anticipate);

//probability for the patient to double click for the previous deco
formula proba_double_click_deco = (0.03 * (1-proba_anticipate - proba_good_not_click));

//probability for the patient to click several time for the previous deco
formula proba_several_click_deco = (0.07 * (1-proba_anticipate - proba_good_not_click));



module patient
	will_play : bool init true;
	front_screen : bool init false;


	anticipate : bool init false;
	click_fast : bool init false;
	click_slow : bool init false;
	not_click : bool init false;
	double_click : bool init false;
	several_click : bool init false;

	curr_train : bool init false;
	curr_targ : bool init false;
	curr_deco : bool init false;
	prev_train : bool init false;
	prev_targ : bool init false;
	prev_deco : bool init false;
	prev_none : bool init false;


	//Start game
	[pressStart] will_play=true & front_screen=false -> (front_screen'=true);

	[transiting] true -> (prev_targ'=curr_targ) & (prev_deco'=curr_deco) & (prev_none'=false) & (prev_train'=curr_train);

	[firstFastTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (several_click'=true);

	[firstMediTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (several_click'=true);

	[firstSlowTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (curr_train'=true) & (several_click'=true);


	[firstFastDeco] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (several_click'=true);

	[firstMediDeco] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (several_click'=true);

	[firstSlowDeco] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_deco'=true) & (curr_train'=true) & (several_click'=true);



	[trainFastTarg] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainMediTarg] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainSlowTarg] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[trainFastTarg] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainMediTarg] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainSlowTarg] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);



	[trainFastDeco] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainMediDeco] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainSlowDeco] will_play=true & front_screen=true & prev_targ=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[trainFastDeco] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainMediDeco] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[trainSlowDeco] will_play=true & front_screen=true & prev_deco=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);



	[fastTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);



	[fastDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=true -> proba_train_anticipate : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_train_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_train_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_train_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (curr_train'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);



	[fastTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDeco] will_play=true & front_screen=true & prev_targ=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDeco] will_play=true & front_screen=true & prev_deco=true & prev_train=false -> proba_anticipate : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastEnd] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediEnd] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowEnd] will_play=true & front_screen=true & prev_deco=true -> proba_anticipate : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_deco : (curr_deco'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[endGame] will_play=true & front_screen=true -> (will_play'=false) & (front_screen'=false) & (prev_targ'=false) & (prev_deco'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false);

	[endLoop] will_play=false & front_screen=false -> true;

endmodule


const int time_betw_sign_max = 3;
const int total_time_max = (num_sign_max+1)*time_betw_sign_max;


module observer

	total_time : [0..total_time_max] init 0;

	time_betw_sign : [0..time_betw_sign_max] init 0;

	prev_time_betw_sign : [0..time_betw_sign_max] init 0;

	num_action : [0..num_sign_max+1] init 0;

	num_act_targ : [0..num_targ_max] init 0;

	num_act_deco : [0..num_deco_max] init 0;

	memo_targ : [0..1] init 0;

	memo_deco : [0..1] init 0;

	transiting : bool init false;

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstFastTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);

	//Signal appeared and it is too soon for the patient to click
	[firstMediTarg] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowTarg] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1);

/////////////////////////////////////////////////////////////////////

	[transiting] true -> (prev_time_betw_sign'=time_betw_sign) & (transiting'=true) & (num_act_targ'=num_act_targ+memo_targ) & (num_act_deco'=num_act_deco+memo_deco);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastDeco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);

	//Signal appeared and it is too soon for the patient to click
	[firstMediDeco] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowDeco] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[trainFastTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

	//Signal appeared and it is too soon for the patient to click
	[trainMediTarg] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[trainSlowTarg] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[trainFastDeco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

	//Signal appeared and it is too soon for the patient to click
	[trainMediDeco] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[trainSlowDeco] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediTarg] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

	//Prepare for end of game
	[slowTarg] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=1) & (memo_deco'=0);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastDeco] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

	//Signal appeared and it is too soon for the patient to click
	[mediDeco] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowDeco] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1) & (memo_targ'=0) & (memo_deco'=1);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastEnd] true -> (total_time'=total_time+1) & (time_betw_sign'=1) & (transiting'=false) & (num_action'=num_action+1);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediEnd] true -> (total_time'=total_time+2) & (time_betw_sign'=2) & (transiting'=false) & (num_action'=num_action+1);

	//Prepare for end of game
	[slowEnd] true -> (total_time'=total_time+3) & (time_betw_sign'=3) & (transiting'=false) & (num_action'=num_action+1);

/////////////////////////////////////////////////////////////////////

	//End loop
	[endLoop] true -> true;

endmodule


rewards "good_on_target"
	prev_targ & (click_fast|click_slow) & transiting=false & prev_train=false: 1;
endrewards

rewards "good_on_decoy"
	prev_deco & (not_click) & transiting=false & prev_train=false: 1;
endrewards

rewards "good_on_signal"
	prev_deco & (not_click) & transiting=false & prev_train=false: 1;
	prev_targ & (click_fast|click_slow) & transiting=false & prev_train=false: 1;
endrewards


rewards "bad_on_target"
	prev_targ & !click_fast & !click_slow & transiting=false & prev_train=false: 1;
endrewards

rewards "bad_on_decoy"
	prev_deco & !not_click & transiting=false & prev_train=false: 1;
endrewards

rewards "bad_on_signal"
	prev_deco & !not_click & transiting=false & prev_train=false: 1;
	prev_targ & !click_fast & !click_slow & transiting=false & prev_train=false: 1;
endrewards



// Probabilité que patient clique une fois sur chaque cible

// Probabilité détats futurs où patient clique plusieurs fois sur decoract

// Probabilité que patient clique plusieurs fois sur chaque decoract

// Probabilité que patient clique plusieurs fois sur chaque signal

// Reward combien de fois il fait juste pour cible

// Reward combien de fois il fait juste pour decoracteur

// Reward combien de fois il fait juste pour signal
