dtmc

const int num_targ_training = 3;
const int num_targ_max = 10 + num_targ_training;
const int num_dist_training = 1;
const int num_dist_max = 5 + num_dist_training;
const int num_sign_max = num_targ_max + num_dist_max;

const int disp_time = 2; // Represents the 300ms of display time for each signals
const int apparition_time = 13; // Represents the 2000ms separating each signal
const int app_time_var = 10; // Represents the 1500ms variation for apparition of each signal (2000 +- 1500)
const int time_betw_sign_max = disp_time+apparition_time+app_time_var; //the maximum time we can have between two pictures
const int time_betw_sign_min = disp_time+apparition_time-app_time_var; //the maximum time we can have between two pictures


//const int click_time_max = 5; // The patient got 1500ms to click on the picture
const int anticipation = 1; // If the patient clicks during the first 150ms, we consider it was anticipation
//const int overlap_max = click_time_max-time_betw_sign_min; //maximum duration of an overlap between clickable time and display of new picture

formula num_sign = num_targ + num_dist;


formula proba_next_targ = (num_targ_max - num_targ) / (num_sign_max - num_sign);
formula proba_next_dist = (num_dist_max - num_dist) / (num_sign_max - num_sign);



module game

	game_on : bool init false; // is the game on or off

	fast_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	medi_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	slow_apparition : bool init false; //This boolean determines the time between the previous picture and the current one

	next_targ : bool init false;
	next_dist : bool init false;

	//num_sign : [0..num_sign_max] init 0; // How many signals did appear
	num_targ : [0..num_targ_max] init 0; // How many signals did appear
	num_dist : [0..num_dist_max] init 0; // How many signals did appear

	next_end : bool init false; // To determine if in next step, it is end of the game


	//Start game
	[pressStart] game_on=false & next_end=false & num_sign=0 -> 1/3*proba_next_targ : (game_on'=true) & (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_dist : (game_on'=true) & (fast_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (game_on'=true) & (medi_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (game_on'=true) & (slow_apparition'=true) & (next_dist'=true);


/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastTarg] game_on=true & num_sign=0 & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediTarg] game_on=true & num_sign=0 & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowTarg] game_on=true & num_sign=0 & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastDist] game_on=true & num_sign=0 & fast_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (fast_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediDist] game_on=true & num_sign=0 & medi_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (medi_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowDist] game_on=true & num_sign=0 & slow_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (slow_apparition'=false) & (next_dist'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition
	[transiting] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3*proba_next_targ : (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_dist : (fast_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (medi_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (slow_apparition'=true) & (next_dist'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (fast_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & medi_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (medi_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & slow_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (slow_apparition'=false) & (next_dist'=false);

/////////////////////////////////////////////////////////////////////
	//Transition to end
	[transiting] game_on=true & num_sign=num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3 : (fast_apparition'=true) + 1/3 : (medi_apparition'=true) + 1/3 : (slow_apparition'=true);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastEnd] game_on=true & num_sign=num_sign_max & fast_apparition=true -> (next_end'=true) & (fast_apparition'=false);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediEnd] game_on=true & num_sign=num_sign_max & medi_apparition=true -> (next_end'=true) & (medi_apparition'=false);

	//Prepare for end of game
	[slowEnd] game_on=true & num_sign=num_sign_max & slow_apparition=true -> (next_end'=true) & (slow_apparition'=false);

/////////////////////////////////////////////////////////////////////
	//End of the game
	[endGame] game_on=true & next_end=true -> (game_on'=false);

	//End loop
	[endLoop] game_on=false & next_end=true -> true;

endmodule


/////////////////////////////////////////////////////////////////////

//probability for the patient to click exactly once for the previous target
formula proba_anticipate = 0.05;


//////////////////////////////////////

//probability for the patient to click exactly once for the previous target
formula proba_good_click_fast = 0.4*(1-proba_anticipate);

//probability for the patient to click exactly once for the previous target
formula proba_good_click_slow = 0.2*(1-proba_anticipate);

//probability for the patient to not click at all for the previous target
formula proba_bad_not_click = 0.05*(1-proba_anticipate);

//probability for the patient to double click for the previous target
formula proba_double_click_targ = 0.34*(1-proba_anticipate);

//probability for the patient to click several time for the previous target
formula proba_several_click_targ = 0.01*(1-proba_anticipate);

//////////////////////////////////////

//probability for the patient to click exactly once for the previous dist
formula proba_bad_click_fast = 0.3*(1-proba_anticipate);

//probability for the patient to click exactly once for the previous dist
formula proba_bad_click_slow = 0.1*(1-proba_anticipate);

//probability for the patient to not click at all for the previous dist
formula proba_good_not_click = 0.5*(1-proba_anticipate);

//probability for the patient to double click for the previous dist
formula proba_double_click_dist = 0.07*(1-proba_anticipate);

//probability for the patient to click several time for the previous dist
formula proba_several_click_dist = 0.03*(1-proba_anticipate);

//////////////////////////////////////

//probability for the patient to not click at all for the previous dist
formula proba_first_not_click = 0.87;

//probability for the patient to click exactly once for the previous dist
formula proba_first_click_slow = 0.03;

//probability for the patient to double click for the previous dist
formula proba_double_click_first = 0.07;

//probability for the patient to click several time for the previous dist
formula proba_several_click_first = 0.03;



module patient
	will_play : bool init true;
	front_screen : bool init false;


	anticipate : bool init false;
	click_fast : bool init false;
	click_slow : bool init false;
	not_click : bool init false;
	double_click : bool init false;
	several_click : bool init false;

	curr_targ : bool init false;
	curr_dist : bool init false;
	prev_targ : bool init false;
	prev_dist : bool init false;
	prev_none : bool init false;



	//Start game
	[pressStart] will_play=true & front_screen=false -> (front_screen'=true);

	[transiting] true -> (prev_targ'=curr_targ) & (prev_dist'=curr_dist) & (prev_none'=false);

	[firstFastTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (several_click'=true);

	[firstMediTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (several_click'=true);

	[firstSlowTarg] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_targ'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_targ'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_targ'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_targ'=true) & (several_click'=true);


	[firstFastDist] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_dist'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_dist'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_dist'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_dist'=true) & (several_click'=true);

	[firstMediDist] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_dist'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_dist'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_dist'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_dist'=true) & (several_click'=true);

	[firstSlowDist] will_play=true & front_screen=true -> proba_first_not_click : (prev_none'=true) & (curr_dist'=true) & (not_click'=true) + proba_first_click_slow : (prev_none'=true) & (curr_dist'=true) & (click_slow'=true) + proba_double_click_first : (prev_none'=true) & (curr_dist'=true) & (double_click'=true) + proba_several_click_first : (prev_none'=true) & (curr_dist'=true) & (several_click'=true);


	[fastTarg] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastTarg] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediTarg] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowTarg] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=true) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastDist] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDist] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDist] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastDist] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediDist] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowDist] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=true) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowEnd] will_play=true & front_screen=true & prev_targ=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_targ : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[fastEnd] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[mediEnd] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);

	[slowEnd] will_play=true & front_screen=true & prev_dist=true -> proba_anticipate : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=true) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_fast : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=true) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_bad_click_slow : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=true) & (not_click'=false) & (double_click'=false) & (several_click'=false) + proba_good_not_click : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=true) & (double_click'=false) & (several_click'=false) + proba_double_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=true) & (several_click'=false) + proba_several_click_dist : (curr_dist'=false) & (curr_targ'=false) & (anticipate'=false) & (click_fast'=false) & (click_slow'=false) & (not_click'=false) & (double_click'=false) & (several_click'=true);


	[endGame] will_play=true & front_screen=true -> (will_play'=false) & (front_screen'=false) & (prev_targ'=false) & (prev_dist'=false);

	[endLoop] will_play=false & front_screen=false -> true;

endmodule


const int total_time_max = (num_sign_max+1)*3;

module observer

	total_time : [0..total_time_max] init 0;

	time_betw_sign : [0..3] init 0;

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstFastTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1);

	//Signal appeared and it is too soon for the patient to click
	[firstMediTarg] true -> (total_time'=total_time+2) & (time_betw_sign'=2);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowTarg] true -> (total_time'=total_time+3) & (time_betw_sign'=3);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastDist] true -> (total_time'=total_time+1) & (time_betw_sign'=1);

	//Signal appeared and it is too soon for the patient to click
	[firstMediDist] true -> (total_time'=total_time+2) & (time_betw_sign'=2);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowDist] true -> (total_time'=total_time+3) & (time_betw_sign'=3);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastTarg] true -> (total_time'=total_time+1) & (time_betw_sign'=1);

	//Signal appeared and it is too soon for the patient to click
	[mediTarg] true -> (total_time'=total_time+2) & (time_betw_sign'=2);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowTarg] true -> (total_time'=total_time+3) & (time_betw_sign'=3);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastDist] true -> (total_time'=total_time+1) & (time_betw_sign'=1);

	//Signal appeared and it is too soon for the patient to click
	[mediDist] true -> (total_time'=total_time+2) & (time_betw_sign'=2);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowDist] true -> (total_time'=total_time+3) & (time_betw_sign'=3);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastEnd] true -> (total_time'=total_time+1) & (time_betw_sign'=1);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediEnd] true -> (total_time'=total_time+2) & (time_betw_sign'=2);

	//Prepare for end of game
	[slowEnd] true -> (total_time'=total_time+3) & (time_betw_sign'=3);

/////////////////////////////////////////////////////////////////////

	//End loop
	[endLoop] true -> true;

endmodule

// Probabilité que patient clique une fois sur chaque cible

// Probabilité détats futurs où patient clique plusieurs fois sur distract

// Probabilité que patient clique plusieurs fois sur chaque distract

// Probabilité que patient clique plusieurs fois sur chaque signal

// Reward combien de fois il fait juste pour cible

// Reward combien de fois il fait juste pour target
