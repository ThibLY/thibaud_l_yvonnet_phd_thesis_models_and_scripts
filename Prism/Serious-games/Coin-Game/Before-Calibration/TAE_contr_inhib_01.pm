dtmc

const int num_targ_training = 3;
const int num_targ_max = 10 + num_targ_training;
const int num_dist_training = 1;
const int num_dist_max = 5 + num_dist_training;
const int num_sign_max = num_targ_max + num_dist_max;

const int disp_time = 2; // Represents the 300ms of display time for each signals
const int apparition_time = 13; // Represents the 2000ms separating each signal
const int app_time_var = 10; // Represents the 1500ms variation for apparition of each signal (2000 +- 1500)
const int time_betw_sign_max = disp_time+apparition_time+app_time_var; //the maximum time we can have between two pictures
const int time_betw_sign_min = disp_time+apparition_time-app_time_var; //the maximum time we can have between two pictures


//const int click_time_max = 5; // The patient got 1500ms to click on the picture
const int anticipation = 1; // If the patient clicks during the first 150ms, we consider it was anticipation 
//const int overlap_max = click_time_max-time_betw_sign_min; //maximum duration of an overlap between clickable time and display of new picture

formula num_sign = num_targ + num_dist;


formula proba_next_targ = (num_targ_max - num_targ) / (num_sign_max - num_sign);
formula proba_next_dist = (num_dist_max - num_dist) / (num_sign_max - num_sign);



module game

	game_on : bool init false; // is the game on or off

	fast_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	medi_apparition : bool init false; //This boolean determines the time between the previous picture and the current one
	slow_apparition : bool init false; //This boolean determines the time between the previous picture and the current one

	next_targ : bool init false;
	next_dist : bool init false;

	//num_sign : [0..num_sign_max] init 0; // How many signals did appear
	num_targ : [0..num_targ_max] init 0; // How many signals did appear
	num_dist : [0..num_dist_max] init 0; // How many signals did appear

	next_end : bool init false; // To determine if in next step, it is end of the game


	//Start game
	[pressStart] game_on=false & next_end=false & num_sign=0 -> 1/3*proba_next_targ : (game_on'=true) & (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (game_on'=true) & (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_dist : (game_on'=true) & (fast_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (game_on'=true) & (medi_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (game_on'=true) & (slow_apparition'=true) & (next_dist'=true);


/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastTarg] game_on=true & num_sign=0 & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediTarg] game_on=true & num_sign=0 & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowTarg] game_on=true & num_sign=0 & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[firstFastDist] game_on=true & num_sign=0 & fast_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (fast_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is too soon for the patient to click
	[firstMediDist] game_on=true & num_sign=0 & medi_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (medi_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[firstSlowDist] game_on=true & num_sign=0 & slow_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (slow_apparition'=false) & (next_dist'=false);

/////////////////////////////////////////////////////////////////////
	//Preparing next apparition
	[transiting] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3*proba_next_targ : (fast_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (medi_apparition'=true) & (next_targ'=true) + 1/3*proba_next_targ : (slow_apparition'=true) & (next_targ'=true) + 1/3*proba_next_dist : (fast_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (medi_apparition'=true) & (next_dist'=true) + 1/3*proba_next_dist : (slow_apparition'=true) & (next_dist'=true);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (fast_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & medi_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (medi_apparition'=false) & (next_targ'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowTarg] game_on=true & num_sign>=1 & num_sign<num_sign_max & slow_apparition=true & next_targ=true -> (num_targ'=num_targ+1) & (slow_apparition'=false) & (next_targ'=false);

/////////////////////////////////////////////////////////////////////
	//Apparition of a new signal
	[fastDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & fast_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (fast_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is too soon for the patient to click
	[mediDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & medi_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (medi_apparition'=false) & (next_dist'=false);

	//Signal appeared and it is time for the patient to click but, a new signal may appear in the next step
	[slowDist] game_on=true & num_sign>=1 & num_sign<num_sign_max & slow_apparition=true & next_dist=true -> (num_dist'=num_dist+1) & (slow_apparition'=false) & (next_dist'=false);

/////////////////////////////////////////////////////////////////////
	//Transition to end
	[transiting] game_on=true & num_sign=num_sign_max & fast_apparition=false & medi_apparition=false & slow_apparition=false & next_end=false -> 1/3 : (fast_apparition'=true) + 1/3 : (medi_apparition'=true) + 1/3 : (slow_apparition'=true);

/////////////////////////////////////////////////////////////////////
	//Signal appeared and it is last instant for the patient to click but, a new signal may appear in the next step
	[fastEnd] game_on=true & num_sign=num_sign_max & fast_apparition=true -> (next_end'=true) & (fast_apparition'=false);

	//Last signal appeared and it is last instant for the patient to click and no signal can appear afterward, we went until the end of click_time_max
	[mediEnd] game_on=true & num_sign=num_sign_max & medi_apparition=true -> (next_end'=true) & (medi_apparition'=false);

	//Prepare for end of game
	[slowEnd] game_on=true & num_sign=num_sign_max & slow_apparition=true -> (next_end'=true) & (slow_apparition'=false);

/////////////////////////////////////////////////////////////////////
	//End of the game
	[endGame] game_on=true & next_end=true -> (game_on'=false);

	//End loop
	[endLoop] game_on=false & next_end=true -> true;

endmodule