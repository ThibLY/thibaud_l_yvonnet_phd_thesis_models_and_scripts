#!/bin/bash

PRISM_CODE_DIR="/home/tlyvonne/Desktop/code/Prism"
cd "../Results/Prism"
RES_DIR=`pwd`
cd $PRISM_CODE_DIR
#echo "Which game do you want to work with?"

while true; do
    echo "Which model family do you want to work with?"
    list_dir=""
    counter=1
    for d in */; do
      echo "($counter) $d"
      counter=$(( $counter + 1 ))
      list_dir="${list_dir} $d"
    done
    read -p " (Type the number)" num
    re='^[0-9]+$'
    if [[ $num =~ $re ]] ; then
        if ((num >= 1 && num <= counter)); then
            set -- $list_dir
            eval "cd \${$num}"
            break
        else
            echo "Number out of range."
        fi
    else
        echo "Enter a number between 1 and $counter"
    fi
done
pwd
model_name=""
re='^[0-9]+$'
while true; do
    echo "Which model do you want to work with?"
    list_model=""
    counter=1
    for m in *.pm; do
      echo "($counter) $m"
      counter=$(( $counter + 1 ))
      list_model="${list_model} $m"
    done
    read -p " (Type the number)" num
    if [[ $num =~ $re ]] ; then
        if ((num >= 1 && num <= counter)); then
            set -- $list_model
            eval "model_name=\${$num}"
            break
        else
            echo "Number out of range."
        fi
    else
        echo "Enter a number between 1 and $counter"
    fi
done
echo ${model_name}
model_suffix=".pm"
properties_suffix=".pctl"
properties_name=${model_name%"$model_suffix"}
properties_name="${properties_name}$properties_suffix"
echo ${properties_name}
re1='^const .*$'
re2='^//.*$'
prop_name_prefix="// "
prob_prop="P.*"
rewa_prop="R.*"
while read line
do
    if [[ $line =~ $re2 ]];then
        prop_name=${line#"$prop_name_prefix"}
        echo -e "==========\n==========\n$prop_name"
    elif [[ $line == '' ]] || [[ $line =~ $re1 ]];then
      :
    else
        echo "yala"
        if [[ $line =~ $prob_prop ]] || [[ $line =~ $rewa_prop ]];then
            echo "yolo"
            echo -e "$line"
            eval "storm --prism ${model_name} --prop '${line}'"
            #printf "$line"
        else
            echo -e "$line"
        fi
    fi
done < $properties_name
# property 7 down  ====> P=? [ (F happy_smiley=true)&(G ((happy_smiley=true)=>(X G happy_smiley=false&quit_game=false))) ]
# Property done nothing not supported =====> P=? [ F ((selection=0)&(inactivity=0)) U (location=location_max) ]
#storm --prism Serious_game.pm --prop "R{\"Happy_smiley_reward\"}=? [ F location=location_max ]"
